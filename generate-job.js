const dependenciesConfig = require('./.dependencies.json');
const yaml = require('js-yaml');
const fs = require('fs');
const _ = require('lodash');

const affected = dependenciesConfig.affectedProjects.filter(d => dependenciesConfig.graph.nodes[d].type === 'app');
const jobs = _.fromPairs(_.map(affected, a => [`job-${a}`, { script: [`npm run ng run ${a}:sonar`] }]));
if (_.isEmpty(jobs)) {
	const yamlText = yaml.safeDump({ 'empty-job': { script: ['echo no effected app'] } });
	console.log(yamlText);
    fs.writeFileSync('.child_job.yml', yamlText, 'utf8');
} else {
    fs.writeFileSync('.child_job.yml',
        yaml.safeDump(_.assign({ image: 'node:12', cache: { key: '${CI_COMMIT_REF_SLUG}', paths: ['node_modules/', 'coverage/', '.dependencies.json'] } }, jobs)),
        'utf8');
}
