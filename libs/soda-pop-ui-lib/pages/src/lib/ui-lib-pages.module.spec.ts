import { async, TestBed } from "@angular/core/testing";
import { UiLibPagesModule } from "./postman-ui-lib-pages.module";

describe("UiLibPagesModule", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UiLibPagesModule]
    }).compileComponents();
  }));

  it("should create", () => {
    expect(UiLibPagesModule).toBeDefined();
  });
});
