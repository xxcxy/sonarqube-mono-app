import { Component, ViewChild, HostBinding, Inject } from '@angular/core';
import { StartupService } from '@pop-soda-facade/postman-ui-lib/services';
import { MatSidenav } from '@angular/material/sidenav';
import { OverlayContainer } from '@angular/cdk/overlay';
import { APP_BASE_HREF } from '@angular/common';

const THEME_DARKNESS_SUFFIX = `-dark`;

@Component({
  selector: 'ui-root',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  @HostBinding('class') activeThemeCssClass: string;
  @ViewChild(MatSidenav) sideNav;

  isThemeDark = false;
  activeTheme: string;

  public isContainerHidden = true;

  constructor (
    private overlayContainer: OverlayContainer,
    private startup: StartupService,
    @Inject(APP_BASE_HREF) public baseHref: string
  ) {
    this.startup.startupData.subscribe(() => {
      this.isContainerHidden = false;
    });
    this.setActiveTheme('deeppurple-amber', false);
  }

  setActiveTheme(theme: string, darkness: boolean = null) {
    if (darkness === null)
      darkness = this.isThemeDark;
    else if (this.isThemeDark === darkness) {
      if (this.activeTheme === theme) return;
    } else
      this.isThemeDark = darkness;

    this.activeTheme = theme;

    const cssClass = darkness === true ? theme + THEME_DARKNESS_SUFFIX : theme;

    const classList = this.overlayContainer.getContainerElement().classList;
    if (classList.contains(this.activeThemeCssClass))
      classList.replace(this.activeThemeCssClass, cssClass);
    else
      classList.add(cssClass);

    this.activeThemeCssClass = cssClass;
  }
}
