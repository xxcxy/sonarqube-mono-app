import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Location, DOCUMENT } from '@angular/common';
import { LoggerService, HttpHelperService, FACADE_API_PREFIX } from '@pop-soda-facade/postman-ui-lib/services';
import get from 'lodash/get';
import moment from 'moment';

@Component({
  selector: 'ui-public-data',
  templateUrl: './public-data.component.html',
  styleUrls: ['./public-data.component.scss']
})
export class PublicDataComponent implements OnInit, OnDestroy {
  
  dataLog: string;
  response: string;
  public locationString = 'London';
  
  constructor(
    private logger: LoggerService,
    private httpHelper: HttpHelperService,
    private location: Location,
    @Inject(DOCUMENT) private document: Document,
    @Inject(FACADE_API_PREFIX) private facadeAPIPrefix
  ) {}

  ngOnInit() {
    this.logger.setLogger(this.addToLog.bind(this));
  }

  async performLocationSearch() {
    this.clearResponseLogs();
    this.clearDataLogs();
    if (!this.locationString) {
      this.addToResponseLog(`Please enter a location.`);
      return;
    }
    try {
      const url = this.location.prepareExternalUrl(`${this.facadeAPIPrefix}/weather/locationsearch/latlong?location=${this.locationString}`);
      this.logger.log(`Requesting data from Public Endpoint: ${url}`);
      const response = await this.httpHelper.get(url);
      const redirectUri = get(response, 'body.redirect');
      this.addToLog(`Checking for redirection: ${!!redirectUri}`);
      if (redirectUri) {
        this.addToLog(`Redirecting to ${redirectUri}`);
        this.document.location.href = redirectUri;
      }
      this.addToResponseLog(JSON.stringify(JSON.parse(response.body), null, 2));
    } catch (e) {
      this.addToLog(`Error: ${e.message}`);
      this.addToResponseLog(`Error: ${e.message}`);
      throw e;
    }
  }

  clearDataLogs() {
    this.dataLog = '';
  }

  clearResponseLogs() {
    this.response = '';
  }

  addToLog(data) {
    this.dataLog = `${this.dataLog || ''}\n[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] ${data}`;
  }

  addToResponseLog(data) {
    this.response = `${this.response || ''}\n[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] ${data}`;
  }

  ngOnDestroy() {
    this.logger.setLogger(() => {});
  }
}
