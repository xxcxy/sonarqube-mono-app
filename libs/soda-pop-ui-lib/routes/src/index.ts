import { Routes } from '@angular/router';
import { PublicDataComponent } from '@pop-soda-facade/soda-pop-ui-lib/pages';

export const ROUTES: Routes = [
  {
    path: 'dashboard',
    component: PublicDataComponent
  },
  {
    path: '',
    pathMatch:'full',
    redirectTo: '/dashboard'
  },
  {
    path: "**",
    redirectTo: "/dashboard",
    pathMatch: 'full'
  }
];
