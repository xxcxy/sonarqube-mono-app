import winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';
import util from 'util';

import config from '@pop-soda-facade/app-config';

// Setting up the winston logger for the app
export const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.printf(info => {
      return `${info.timestamp} [${info.level}]: ${info.message}`;
    })
  ),
  level: config.WINSTON_LOGGER_LEVEL,
  transports: [
    new DailyRotateFile({ dirname: 'logs', filename: 'error.log', level: 'error' }),
    new DailyRotateFile({ dirname: 'logs', filename: 'combined.log' }),
    new winston.transports.Console()]
});

/**
 * Log error details with signature.
 * @param err the error
 * @param signature the signature
 * @private
 */
export function logFullError(err: Error, signature: string) {
  if (!err['logged']) {
    logger.error(`Error happened in ${signature}:`);
    logger.error(util.inspect(err));
    err['logged'] = true;
  }
}
