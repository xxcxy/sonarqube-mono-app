import winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';

import config from '@pop-soda-facade/app-config';

// Setting up the winston logger for the app
const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.printf(info => {
      return `[${info.timestamp}\t: ${info.message}`;
    })
  ),
  level: config.WINSTON_LOGGER_LEVEL,
  transports: [
    new DailyRotateFile({ dirname: 'logs', filename: 'requests.log', level: 'info' }),
    new winston.transports.Console()]
});

export async function logRequest(request) {
  const response = request.response;
  let str = `${request.method.toUpperCase()} ${request.path}`;
  if (response.variety === 'file') {
    return;
  } else if (response.variety === 'stream') {
    str = `Proxy: ${str}`;
  } else {
    str = `API: ${str}`;
    request.app.payload = JSON.stringify(response.source);
  }
  logger.info(str);
  logger.info(`${str} | Headers: ${JSON.stringify(request.headers)}`);
  logger.info(`${str} | Response: ${request.app.payload}`);
  if (request.payload !== null) {
    logger.info(`${str} | Request Payload: ${JSON.stringify(request.payload)}`);
  }
}