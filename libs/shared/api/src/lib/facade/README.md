This folder contains the openAPI specifications for the facade endpoints.

Each API endpoint that is configured in the hapi-facade service  
should have a a corresponding openAPI file (using the openAPI 3.0.x format).

These `swaggers`  should be in the yaml format to enable good documentation (internal comments ).

Each swagger file will identify the Request / Response data attributes for the api -
this is *only* the requirements for I/O from the client to the hapi-facade-api.

Each swagger file will be processed by the openApi-generator-cli to generate a collection of typescript classes and interfaces that describe the service.
These classes can then be imported into the client application, or into
the lambda transformation functions used by the hapi-facade-api.