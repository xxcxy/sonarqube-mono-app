/**
 * locationSearch API
 * Facade API Swagger for `locationSearch` endpoint
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface Location { 
    woeid?: number;
    latt_long?: string;
    title?: string;
    /**
     * location type
     */
    location_type?: Location.LocationTypeEnum;
}
export namespace Location {
    export type LocationTypeEnum = 'City' | 'State' | 'Country';
    export const LocationTypeEnum = {
        City: 'City' as LocationTypeEnum,
        State: 'State' as LocationTypeEnum,
        Country: 'Country' as LocationTypeEnum
    };
}


