// @ts-ignore
import * as path from 'path';
// @ts-ignore
import * as fs from 'fs';

export { Location as MetaWeatherResponse } from './lib/generated/api.metaweather/model/location';
export { LocationV2 as LambdaLocationV2Response } from './lib/generated/api.locationsearch-latlong/model/locationV2';

export const swaggerRootDir = path.resolve('libs', 'shared', 'api', 'src', 'lib', 'facade');

export function getSwaggerFilePath(fileName: string) {
    const filePath = path.join(swaggerRootDir, fileName);

    try {
        if (fs.existsSync(filePath)) {
            return filePath;
        }
    } catch(err) {
        throw err;
    }
}
