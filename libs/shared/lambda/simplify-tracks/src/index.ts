export async function simplifyTracks(event) {
  const body = JSON.parse(event.body);
  const result = {
    id: body.id,
    name: body.name,
    description: body.description,
    tracks: body.tracks.items.map((track) => {
      return {
        id: track.track.id,
        name: track.track.name,
        uri: track.track.uri
      };
    })
  };

  const response = {
    statusCode: 200,
    body: JSON.stringify(result)
  };

  return response;
}
