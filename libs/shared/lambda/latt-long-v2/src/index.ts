import axios from 'axios';
import _ from 'lodash';

/**
 * Lambda function that performs a reverse latt-long-v2 lookup
 * on the provided list of co-ordinates
 * @param event Lambda event
 * @param event.body Body that is sent with the request
 * @returns an Object with statusCode and body property
 */
export async function latLongV2 (event) {
  // Try to parse the JSON
  try {
    event = JSON.parse(event.body);
  } catch {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: 'Request body couldn\'t be parsed. Ensure that it is in JSON format.'
      })
    };
  }
  try {
    // Validation of the response body
    if (!(event instanceof Array)) {
      return {
        statusCode: 400,
        body: JSON.stringify({
          message: 'Request body must be an array'
        })
      };
    }
    // Define the r
    const url = 'https://maps.googleapis.com/maps/api/geocode/json';
    event = await Promise.all(event.map(async (item, idx) => {
      const latLong = item.latt_long;
      if (!latLong || !(typeof(latLong) === 'string')) {
        return {
          statusCode: 400,
          message: `Property latt_long not found or not a string at index: ${idx}.`
        };
      }

      //delete item.latt_long; // TODO: Uncomment for Demo

      let coOrdinates: Array<Number|string> = latLong.split(',');
      coOrdinates = coOrdinates.map((val, subIdx) => {
        const numVal = Number(val);
        if (numVal === NaN) {
          const err = new Error(`Invalid co-ordinate ${val} for item at index ${idx}`);
          err['statusCode'] = 400;
          throw err;
        }
        const absNumVal = Math.abs(numVal);
        const isInvalid = (subIdx === 0) ? absNumVal > 90 : absNumVal > 180;
        if (isInvalid) {
          const err = new Error(`Invalid co-ordinate ${val} for item at index ${idx}`);
          err['statusCode'] = 400;
          throw err;
        }
        return numVal.toFixed(6);
      });
      const [ latitude, longitude ] = coOrdinates;
      const itemInfo = await axios.get(url, {
        params: {
          latlng: `${latitude},${longitude}`,
          key: 'AIzaSyCxOxViVzR_VWY4An0-kNBsCXeAKQXBatk',
        }
      });
      const geoInformation = _.get(itemInfo, 'data.results[0]');
      return {
        ...item,
        latitude,
        longitude,
        //lambda_version: 'v2',  // TODO: Uncomment for Demo
        info: geoInformation
      };
    }));
    return {
      statusCode: 200,
      body: JSON.stringify(event)
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: err.statusCode || 500,
      body: JSON.stringify({
        message: (err && err.message) || 'Internal server error'
      })
    };
  }
}
