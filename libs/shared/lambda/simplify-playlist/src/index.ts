/**
 * Handler function simplify the output of the playlist.
 * @param event the serverless event. It is expected to have a body containing the playlist to simplify.
 * @returns {Promise<{body, statusCode: number}>}
 */
export async function simplifyPlaylist(event) {
  const body = JSON.parse(event.body);
  body.items = body.items.map((item) => {
    return {
      id: item.id,
      name: item.name,
      description: item.description,
      tracks: item.tracks
    };
  });

  const response = {
    statusCode: 200,
    body: JSON.stringify(body)
  };

  return response;
}
