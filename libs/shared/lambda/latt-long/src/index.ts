import { MetaWeatherResponse, LambdaLocationV2Response } from '@pop-soda-facade/shared/api';
import * as _ from 'lodash';

/**
 * lambda function to transform latt_long into two properties.
 * @param {Array} event array of numbers
 * @returns array sum
 */
export async function latLong(event) {
  const weatherLocation: Array<MetaWeatherResponse> = JSON.parse(event.body);
  const responseBody: Array<LambdaLocationV2Response> = [];
  for (let x = 0; x < event.length; x++) {
    const coordinateArray = weatherLocation[x].latt_long.split(',');
    event[x].latitude = coordinateArray[0];
    event[x].longitude = coordinateArray[1];

    //event[x].lambda_version = "v1"; // TODO: Uncomment for Demo

    responseBody.push(_.omit(event[x], 'latt_long'));
  }

  const response = {
    statusCode: 200,
    body: JSON.stringify(responseBody)
  };
  return response;
}
