import {latLong} from './index';

describe('Lambda: latLong', function () {

  const payload = [
    {latt_long: '1,2'},
    {latt_long: '2,3'},
    {latt_long: '3,4'}
  ];

  describe('when a properly formatted payload is received', () => {

    test('it should return body and statusCode == 200', async function () {

      const event = {body: JSON.stringify(payload)};

      const response = await latLong(event);

      expect(response).toHaveProperty('body');
      expect(response).toHaveProperty('statusCode', 200);
    });

    test('it should return response array of objects with longitude and latitude properties', async function () {

      const event = {body: JSON.stringify(payload)};

      const response = await latLong(event);

      const body = JSON.parse(response.body);
      body.forEach((actual, index) => {
        const expected = payload[index].latt_long.split(',');
        expect(actual).toHaveProperty('latitude', `${expected[0]}`);
        expect(actual).toHaveProperty('longitude', `${expected[1]}`);
      });

    });

  });


});