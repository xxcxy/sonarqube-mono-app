import * as _ from 'lodash';
import * as fs from 'fs';
import { resolve } from 'path';

const facadeServiceEnv = process.env.FACADE_SERVICE_ENV || 'local';

const configFiles = [
  'server',
  'facade-service'
];

const appConfig: any = {};

_.each(configFiles, (configFileName) => {
  const filePath = resolve(__dirname, `../../../dist/apps/facade-config/${facadeServiceEnv}/${configFileName}.json`);
  let { config } = JSON.parse(fs.readFileSync(filePath, 'utf-8'));
  config = _.reduce(config, (acc, val, key) => {
    let value = val;
    if (val.type === 'filePath') {
      value = fs.readFileSync(resolve(__dirname, val.value), 'utf-8');
    }
    acc[key] = value;
    return acc;
  }, {});
  _.merge(appConfig, config);
});

export default appConfig;
