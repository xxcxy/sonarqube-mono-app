const jose = require("jose");

const JWK = jose.JWK;
const testKey = JWK.generateSync("EC", "P-256");
const jwk = testKey.toJWK();

class KeyService {
  jwk() {
    return jwk;
  }
  sign(data) {
    return jose.JWT.sign(data, testKey, {
      algorithm: "ES256",
      header: {
        typ: "JWT",
        alg: "ES256",
        jwk: { kty: jwk.kty, crv: jwk.crv, x: jwk.x, y: jwk.y },
        kid: jwk.kid
      }
    });
  }
}
export const keyService = new KeyService();
