import { Schema, TestRequest, TestResponse } from './types';
import { httpService } from './http-helper/http-helper.service';
import { keyService } from './key/key.service';
import { get, find, map } from 'lodash';

const SwaggerParser = require('@apidevtools/swagger-parser');
const cookie = require('cookie');

export interface APIOptions {
  baseURL: string;
  authURL: string;
  swaggerFile: string;
  protected?: boolean;
  loginURL?: string;
  loginUsername?: string;
  loginPassword?: string;
}

export class SwaggerAPI {
  private _api: any;
  private _anonymousToken: string | null;
  private _idToken: string | null;

  constructor(private options: APIOptions) {}

  private async getApi() : Promise<any> {
    if (this._api) {
      return this._api;
    }
    this._api = await this.parseSwaggerFile();
    this._api = Object.freeze(this._api);
    return this._api;
  }

  public async getSchemaFor(method: string, url: string,  statusCode: Number) : Promise<Schema> {
    const api = await this.getApi();

    // find the api object for the given url from the swagger
    const path = get(api.paths, [url]);
    if (!path) {
      throw new Error(
          `No Such API: ${method.toUpperCase()} ${url}`
      );
    }

    // find the api endpoint for the given method type [GET,POST,PUT,DEL]
    const endpoint = find(path, (_val, key) => key.toLowerCase() === method.toLowerCase());
    if (!endpoint) {
      throw new Error(
          `No Such API Error: ${method.toUpperCase()} ${url}`
      );
    }

    const expectedResponse = get(endpoint, [
      'responses',
      `${statusCode}`,
      'content',
      'application/json',
      'schema'
    ]);
    return expectedResponse;
  }

  async parseSwaggerFile() {
    try {
      const res = await SwaggerParser.validate(this.options.swaggerFile);
      return res;
    } catch (err) {
      console.error(err);
    }
  }

  // Testing Anonymous API requires a valid `a_token` cookie
  async getAnonymousToken() {
    if (this._anonymousToken) {
      return this._anonymousToken;
    }
    const response = await httpService.request('', null, null, {
      method: 'POST',
      url: this.options.authURL,
      data: {
        publicKey: keyService.jwk()
      }
    });

    const cookieStrings = response.headers['set-cookie'];
    for (const cookieString of cookieStrings) {
      if (!cookieString) {
        continue;
      }
      const cookies = cookie.parse(cookieString);
      const a_token = cookies['a_token'];
      this._anonymousToken = Object.freeze(a_token);
       return this._anonymousToken;
    }
    return null;
  }


  async getAuthenticatedToken() {
    if (this._idToken) {
      return this._idToken;
    }
    const anonymousToken = await this.getAnonymousToken();
    const response = await httpService.request('', anonymousToken, null, {
      method: 'POST',
      url: this.options.loginURL,
      data: {
        username: this.options.loginUsername,
        password: this.options.loginPassword
      }
    });

    let cookies = response.headers['set-cookie'];
    cookies = map(cookies, cookie.parse);
    for (const cookieItem of cookies) {
      if (!cookieItem || !cookieItem.id_token) {
        continue;
      }
      const id_token = cookieItem['id_token'];
      this._idToken = id_token;
      this._idToken = Object.freeze(this._idToken);
      return this._idToken;
    }
    return null;
  }

  async send(request: TestRequest): Promise<TestResponse> {
    const response = await httpService.request(
      this.options.baseURL,
      this.options.protected ? null : (await this.getAnonymousToken()),
      this.options.protected ? (await this.getAuthenticatedToken()) : null,
      request
    );

    return response as TestResponse;
  }
}
