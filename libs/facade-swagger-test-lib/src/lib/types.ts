export interface TestRequest {
  method: string;
  url: string;
}

export interface TestResponse {
  status: number;
  data?: any;
  headers?: any;
  //schemas: { [status: number]: Schema };
}

export type Schema = any;