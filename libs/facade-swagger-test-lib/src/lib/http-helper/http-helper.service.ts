import { merge, forEach } from "lodash";
import { keyService } from "../key/key.service";
import * as stableStringify from "json-stable-stringify";
import axios from "axios";
import * as https from "https";
import * as crypto from "crypto";

type HttpMethods = "get" | "post" | "put" | "patch" | "delete";
const instance = axios.create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
});
export class HttpHelperService {
  logger: any;
  tmoWid = "testWid";
  constructor() {
    this.logger = {
      log: () => {}
    };
  }

  /**
   * Generates SHA256 Fingerprint for a given string
   * @param string String 
   */
  async generateSHA256Fingerprint(string) {
    return crypto
      .createHash("sha256")
      .update(string)
      .digest("hex");
  }

  /**
   * Generates a nonce for an HTTP request, given it's method, URL and body.
   * @param method Request Method
   * @param url Request URL
   * @param body Request Body
   */
  async generateNonce(
    method: HttpMethods,
    url: string,
    params: any,
    body: any = {}
  ) {
    const ehts = ["method", "uri"];
    let edts = `${method}${url}`;
    if (params) {
      let queryParams = "";
      forEach(params, (value, key) => {
        queryParams += queryParams === "" ? "?" : "&";
        queryParams += `${key}=${encodeURIComponent(value)}`;
      });
      edts = `${edts}${queryParams}`;
    }
    if (method !== "get") {
      ehts.push("body");
      edts = `${edts}${stableStringify(body)}`;
    }
    this.logger.log(`ehts: ${JSON.stringify(ehts, null, 2)}`);
    this.logger.log(`edts string: ${edts}`);
    const edtsFingerprint = await this.generateSHA256Fingerprint(edts);
    this.logger.log(`edts fingerprint: ${edtsFingerprint}`);
    const nonce: any = { ehts, edts: edtsFingerprint };
    this.logger.log(`Nonce: ${JSON.stringify(nonce)}`);
    return keyService.sign(nonce);
  }

  /**
   * Helper method to create a request to a server using POP JWT authentication
   * @param options Request options 
   */
  async request(baseURL: string, a_token: string | null, id_token: string | null, options: any) {
    const newOptions = {
      ...options
    };
    const method = newOptions.method.toLowerCase();
    const url = newOptions.url;
    const fullUrl = `${baseURL}${url}`;
    const payload = newOptions.data;
    const nonce = await this.generateNonce(
      method,
      fullUrl,
      newOptions.params,
      payload
    );
    this.logger.log(`Nonce: ${nonce}`);
    const wid = this.tmoWid;
    this.logger.log(`Options: ${JSON.stringify(newOptions)}`);
    //merge(options.header, { headers: { 'x-nonce': nonce, 'x-wid': wid }, observe: 'response' });
    const headers = { "x-nonce": nonce, "x-wid": wid };
    if (a_token) {
      headers["Cookie"] = `a_token=${a_token}`;
    }
    if (id_token) {
      headers["Cookie"] = `id_token=${id_token}`;
    }
    merge(newOptions, {
      method,
      url: fullUrl,
      headers,
      observe: "response"
    });
    if (!newOptions.headers["Content-Type"]) {
      newOptions.headers["Content-Type"] = "application/json";
    }
    this.logger.log(
      `${method.toUpperCase()} ${url}: ${JSON.stringify(
        payload
      )} : ${JSON.stringify(newOptions)}`
    );
    const res: any = await instance(newOptions);
    this.logger.log(`Response: ${JSON.stringify(res.data, null, 2)}`);
    this.logger.log(
      `Headers: ${JSON.stringify(res.headers.normalizedNames, null, 2)}`
    );
    return res;
  }
}
export const httpService = new HttpHelperService();
