# facade-service-api-plugin

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test facade-service-api-plugin` to execute the unit tests via [Jest](https://jestjs.io).
