import boom from "@hapi/boom";
import Hapi from "@hapi/hapi";
import Call from '@hapi/call';

import _ from "lodash";
import { resolve as pathResolve } from 'path';
import { readFileSync, watchFile } from 'fs';

import apis, { Route } from "./apis";
import controllers from "./controllers";

import config from "@pop-soda-facade/app-config";
import { logFullError, logger } from "@pop-soda-facade/utils";
import buildHandlers from "./services/FacadeService";
import { APIAuthChecker } from "@pop-soda-facade/facade-service/helper";

let currentFacadeConfig = null;

let router = new Call.Router();
let updatedRouter;


/**
 * Register a route with the server
 * @param server the server object
 * @param route the route object
 */
function registerRoute(server: Hapi.Server, route: Route) {
  // Define the route path
  const path = `/api${route.path}`;
  // Log the route path
  logger.info(`Route: [${route.method}] ${path} [Domain: ${config.DOMAIN}]`);
  // Register the route with the server
  server.route({
    method: route.method,
    path,
    handler: (request, h) => handleRoute(route, request, h),
    options: (route.options || {}) as any
  });
}

/**
 * This function is called every time a request is made
 * @param path request path
 * @param functionName function name of this handler
 * @handler handler request handler function
 * @param request the Request object
 * @param response the Response object
 */
async function handleRequest(
  functionName: string,
  handler: Function,
  request: Hapi.Request,
  response: Hapi.ResponseToolkit
) {
  try {
    // Get the result from the controller function
    const result = await handler(request, response);
    // Return the response
    if (result.app && result.unstate) {
      return result;
    }
    const res = response
      .response(result.body || {})
      .code(result.body ? 200 : 204); // 204 for No Content

    // Set headers
    _.forIn(result.header || {}, (value, key) => {
      res.header(key, value);
    });

    return res;
  } catch (err) {
    // Errors are handled below
    // Log the error
    logFullError(err, `${functionName}`);
    // If error is caused by boom, then throw it
    if (boom.isBoom(err)) {
      throw err;
    }
    // Set appropriate status code
    const statusCode = err.statusCode || err.status || 500;
    // Set appropriate status response
    const errResponse = {
      statusCode,
      message: _.defaultTo(err.message, "An internal server error occurred")
    };
    // Return the response and status code
    return response.response(errResponse).code(statusCode);
  }
}

async function handleFacadeRequest(
  facadeService: any,
  request: Hapi.Request,
  response: Hapi.ResponseToolkit
) {
  const { path, functionName, handler } = facadeService;
  logger.info(`${request.method.toUpperCase()} ${path}`);

  // check the token
  const authenticated = await APIAuthChecker.authenticateRequest(
    request,
    !facadeService.protected,
    response
  );
  if (!authenticated) {
    // return 401
    return response.response({ message: "Not Authenticated" }).code(401);
  }
  return handleRequest(functionName, handler, request, response);
}
/**
 * This function is called every time a request is made
 * @param route the API server object
 * @param request the Request object
 * @param response the Response object
 */
async function handleRoute(
  route: Route,
  request: Hapi.Request,
  response: Hapi.ResponseToolkit
) {
  return handleRequest(
    `${route.controller}#${route.function}`,
    controllers[route.controller][route.function],
    request,
    response
  );
}

/**
 * Register Facade routes
 * @param server the server object
 */
function registerFacadeRoute(server: Hapi.Server) {
  const facadeConfigFilePath = pathResolve(__dirname, config.FACADE_CONFIG_PATH);
  const facadeConfig = JSON.parse(readFileSync(facadeConfigFilePath, 'utf-8'));
  const handlers = buildHandlers(facadeConfig.endpoints);

  _.each(handlers, facadeService => registerIndividualFacadeRoute(facadeService, router));

  server.ext("onPreResponse", async (request, h) => {
    const routePath = request.path;
    const routeMethod = request.method;
    const routeInfo = router.route(routeMethod, routePath);
    if (!_.isError(routeInfo) && _.isFunction(routeInfo.route)) {
      _.set(request, 'params', routeInfo.params);
      _.set(request, 'paramsArray', routeInfo.paramsArray);
      const routeHandler = routeInfo.route;
      return routeHandler(request, h);
    }
    return h.continue;
  });

  server.route({
    path: '/{param*}',
    method: '*',
    handler: (_req, _resp) => {
      throw boom.notFound();
    }
  });

  // register file watcher
  currentFacadeConfig = facadeConfig;
  watchFile(facadeConfigFilePath, watcherFunction());
}

function registerIndividualFacadeRoute(facadeService: any, targetRouter: any) {
  logger.info(
    `Route: [${facadeService.method.toUpperCase()}] ${facadeService.path}`
  );
  // Register the route with the server

  const method = facadeService.method || "get";
  const path = facadeService.path;
  const handler = (request, h) => handleFacadeRequest(
    facadeService,
    request,
    h
  );
  targetRouter.add({ method, path }, handler);
}

function watcherFunction() {
  return () => {
    try {
      const facadeConfigFilePath = pathResolve(__dirname, config.FACADE_CONFIG_PATH);
      const newFacadeConfig = JSON.parse(readFileSync(facadeConfigFilePath, 'utf-8'));

      updatedRouter = new Call.Router();
      let handlers = buildHandlers(newFacadeConfig.endpoints);
      _.each(handlers, facadeService => registerIndividualFacadeRoute(facadeService, updatedRouter));

      // find modified routes
      const differences = _.differenceWith(newFacadeConfig.endpoints, currentFacadeConfig.endpoints, _.isEqual);
      handlers = buildHandlers(differences);
      _.each(handlers, (facadeService) => {
        logger.info(
          `Updated Route: [${facadeService.method.toUpperCase()}] ${facadeService.path}`
        );
      });
      // find routes to remove
      const removed = _.differenceWith(currentFacadeConfig.endpoints, newFacadeConfig.endpoints, (a: any, b: any) => {
        return a.method.toUpperCase() === b.method.toUpperCase() && a.uri === b.uri;
      });
      _.each(removed, (endpoint) => {
        logger.info(
          `Removed Route: [${endpoint.method.toUpperCase()}] ${endpoint.uri}`
        );
      });
      router = updatedRouter;

      currentFacadeConfig = newFacadeConfig;

      if (differences.length === 0 && removed.length === 0) {
        logger.info('The facade APIs are not changed.');
      }
    } catch (e) {
      logFullError(e, 'watcherFunction');
    }
  };
}


export const APIPlugin = {
  name: "APIPlugin",
  register: (server: Hapi.Server) => {
    // Add api routes
    _.each(apis, api => registerRoute(server, api));
    // add facade services
    registerFacadeRoute(server);
  }
};
