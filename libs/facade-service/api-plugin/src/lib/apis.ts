import { RouteOptions } from "hapi";

export interface Route {
  method: 'GET' | 'HEAD' | 'POST' | 'PUT' | 'DELETE' | 'CONNECT' | 'OPTIONS' | 'TRACE' | 'PATCH';
  path: string;
  controller: string;
  function: string;
  options: RouteOptions;
}

export default <Array<Route>>[
  {
    method: 'POST',
    path: '/login',
    controller: 'LoginController',
    function: 'postLogin'
  },
  {
    method: 'POST',
    path: '/logout',
    controller: 'LogoutController',
    function: 'postLogout'
  },
  {
    method: 'POST',
    path: '/auth',
    controller: 'AuthController',
    function: 'postAuth'
  }
];
