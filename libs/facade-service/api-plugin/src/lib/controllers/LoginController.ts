import config from '@pop-soda-facade/app-config';
import Boom, { boomify } from '@hapi/boom';
import jwt from 'jsonwebtoken';
import * as _ from 'lodash';
import { logger } from '@pop-soda-facade/utils';
import { get } from 'lodash';
import { TokenHelper, IDTokenHelper } from '@pop-soda-facade/facade-service/helper';

/**
 * Handles POST /api/login
 * @param req Request
 * @param resp Response
 */
async function postLogin(req, resp) {
  try {
    // Get username, password and anonymous token and nonce from the request.
    const { username, password } = req.payload;
    const token = get(req, 'state.id_token') || get(req, 'state.a_token');
    const nonce = req.headers['x-nonce'];

    // Get the client ID for the server.
    const clientId = config.CLIENT_ID;
    const domain = config.DOMAIN;
    
    // Sanity/existence checks.
    const checks = _.every({ nonce, username, password, token, clientId, domain }, (item, key) => {
      const check = _.isString(item) && item.length > 0;
      if (!check) {
        logger.error(`Login failed: ${key} not found`);
      }
      return check;
    });
    // Check(s) failed. Handle it.
    if (!checks) {
      throw Boom.badRequest();
    }

    // Decode the anonymous token.
    const decoded: any = jwt.decode(token);
    // Make sure the CNF is valid, and public certificate is present.
    if (!decoded.cnf[domain]) {
      throw Boom.unauthorized('Invalid cnf.');
    }

    // Check nonce
    await TokenHelper.checkNonce(req, decoded.cnf[domain]);
    // Check validity and refresh token if required
    await TokenHelper.verifyAndRefreshToken(token);
    // Ensure the username and password pair is valid
    const userInfo = _.find(config.USERS, { USERNAME: username, PASSWORD: password });
    if (!userInfo) {
      throw Boom.unauthorized('Invalid username and/or password');
    }
    // Generate an ID token and update the anonymous token.
    const payload = {
      username,
      aud: clientId,
      cnf: decoded.cnf,
      iss: `${req.server.info.protocol}://${req.info.host}/`
    };
    const idToken = await IDTokenHelper.generateIdToken(payload);
    // Respond with the ID token and the updated anonymous token.
    resp.unstate('a_token');
    resp.unstate('id_token');
    resp.state('id_token', idToken );
    return resp.response({ status: 'OK' });
  } catch (e) {
    if (Boom.isBoom(e)) {
      throw e;
    }
    throw Boom.unauthorized(e.message);
  }
}

export default {
  postLogin
};