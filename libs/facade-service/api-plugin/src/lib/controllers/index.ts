
import AuthController from './AuthController';
import LoginController from './LoginController';
import LogoutController from './LogoutController';

export default {
  AuthController,
  LoginController,
  LogoutController
};