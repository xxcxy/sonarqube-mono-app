import { get } from 'lodash';
import config from '@pop-soda-facade/app-config';
import jwt from 'jsonwebtoken';
import { TokenHelper } from '@pop-soda-facade/facade-service/helper';

async function postLogout(req, res) {
  const token = get(req, 'state.id_token');
  if (!token || !TokenHelper.isIdToken(token)) {
    return { status: 'OK' };
  }
  const domain = config.DOMAIN;

  const decoded: any = jwt.decode(token);
  // Check nonce
  await TokenHelper.checkNonce(req, decoded.cnf[domain]);

  // Check validity and refresh token if required
  await TokenHelper.verifyAndRefreshToken(token);

  const usn = get(req, 'state.usn');
  const a_token = await TokenHelper.anonymousTokenFromIdToken(usn, token);
  res.unstate('id_token');
  res.state('a_token', a_token);
  return res.response({ status: 'OK' });
}

export default {
  postLogout
};