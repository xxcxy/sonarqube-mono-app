import { isNil, get, isEqual, has, every, partial } from 'lodash';
import Boom from '@hapi/boom';
import config from '@pop-soda-facade/app-config';
import { TokenHelper } from '@pop-soda-facade/facade-service/helper';

/**
 * Handle the POST /api/auth
 * @param req Request object
 * @param resp Response object
 */
async function postAuth (req, resp) {
  // Get the public key from the request payload
  const publicKey = get(req, 'payload.publicKey');

  const clientId = config.CLIENT_ID;
  const domain = config.DOMAIN;

  const nonce = req.headers['x-nonce'];
  // Existence checks
  every({publicKey, clientId, nonce}, (value, key) => {
    if (isNil(value)) {
      throw Boom.badRequest(`Missing item: ${key}.`);
    }
    return true;
  });
  // Get token from request
  let token = get(req, 'state.a_token');
  // If it isn't found, generate one and return
  if (isNil(token)) {
    // Check nonce
    await TokenHelper.checkNonce(req, publicKey);
    // Generate anonymous token
    return TokenHelper.generateNewUserToken(clientId, domain, publicKey, req, resp);
  }
  // Token was found
  // Check validity and refresh token if required
  const tokens = await TokenHelper.verifyAndRefreshToken(token);
  token = tokens.token;
  const decodedToken = tokens.decodedToken;
  // Determine the cookieName for the token
  const cookieName = 'a_token';
  // Set the token
  resp.unstate(cookieName);
  resp.state(cookieName, token, {
    domain: req.headers.host
  });
  // Check if cnf exists for the current app
  const cnfCert = get(decodedToken, ['cnf', domain]);
  if (isNil(cnfCert)) {
    // Check nonce
    await TokenHelper.checkNonce(req, publicKey);
    // Generate anonymous token
    return TokenHelper.generateNewUserToken(clientId, domain, publicKey, req, resp);
  }
  // If the cnf already exists, check nonce first
  await TokenHelper.checkNonce(req, cnfCert);
  // Check if cnf stored in token is the same as the one passed in.
  if (!isEqual(cnfCert, publicKey)) {
    return TokenHelper.generateNewUserToken(clientId, domain, publicKey, req, resp);
  }
  return { status: 'OK '};
}

export default {
  postAuth
};