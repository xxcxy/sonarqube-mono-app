/**
 * Constants used in facade services.
 */

export const PROCESSOR_TYPES = {
  API: 'api',
  AGGREGATOR: 'transformer'
};

