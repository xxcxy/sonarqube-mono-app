import Joi from '@hapi/joi';
import _ from 'lodash';

import { logger } from '@pop-soda-facade/utils';
import serviceOrder from './DependencyGraph';
import { PROCESSOR_TYPES } from '../constants';
import executeService from './ExecutorService';
import config from '@pop-soda-facade/app-config';

/**
 * Build dependency graph.
 *
 * @param service the service
 * @returns dependency graph
 */
function buildGraph(service) {
  logger.info(`Building dependency graph for endpoint ${service.method.toUpperCase()} ${config.FACADE_SERVICE_API_PREFIX}${service.uri}`);

  // for every processor, get its dependency, and build dependency graph
  const graph = [];
  _.each(service.processors, processor => {
    const processorName = processor.name;
    _.each(processor.dependsOn, node => {
      graph.push([node, processorName]);
    });
  });

  return graph;
}

// service schema. Joi will validate this schema.
const serviceSchema = Joi.array().items(Joi.object().keys({
  uri: Joi.string().required(),
  method: Joi.string().optional(),
  protected: Joi.boolean().optional().default(false),
  processors: Joi.array().items(Joi.object().keys({
    name: Joi.string().required(),
    type: Joi.string().required().valid(...Object.values(PROCESSOR_TYPES)),
    uri: Joi.string().optional(),
    dependsOn: Joi.array().items(Joi.string().required()).optional(),
    request: Joi.object().keys({
      method: Joi.string().optional(),
      params: Joi.any().optional(),
      body: Joi.any().optional(),
      header: Joi.any().optional()
    }).optional(),
    response: Joi.object().keys({
      body: Joi.any().optional(),
      header: Joi.any().optional()
    }).optional(),
  })).unique('name').required(),
}));

/**
 * Check service config correctness. Throw error if config is not correct.
 *
 * @param serviceConfig service config
 * @param serviceEnv service environment (e.g local, prod).
 * @returns service execution order
 */
function checkConfig(serviceConfig) {
  logger.info('Validating facade service configs');

  // validate input
  serviceSchema.validate(serviceConfig);
  const result = [];

  for (const service of serviceConfig) {
    // build dependency graph
    const graph = buildGraph(service);
    // resolve dependency graph and get service execution order
    const order = serviceOrder(graph);

    result.push({
      config: service,
      order,
    });
  }
  return result;
}

/**
 * Run services in parallel.
 * @param serviceConfigs service configs
 * @param context execution context
 * @returns updated context and final result if any
 */
async function runServicesParallel(serviceConfigs, context) {
  // promise to run these services
  const executions = _.map(serviceConfigs, singleServiceConfig => executeService(singleServiceConfig, context));
  // build new context
  const layerResult: any = {};
  // run all services in parallel and return new context and final result if any
  return Promise.all(executions).then(results => {
    _.each(results, (result: any) => {
      layerResult[result.name] = result.result;
    });
    return layerResult;
  }).catch(err => {
    throw err;
  });
}

/**
 * Handler one service.
 * @param serviceConfig service config
 * @param order execution order
 * @param request http request from client
 * @returns final result of this service
 */
async function serviceHandler(serviceConfig, order, request) {
  const context: any = {};

  // read params from request and put into context
  // currently cares about "path", "query", "headers" and "payload" parameters
  context.request = _.pick(request, 'params', 'query', 'headers');

  // Use "body" instead of "payload"
  context.request.body = _.get(request, 'payload', {});

  if (order.length <= 0) {
    // there are some processors not in dependency graph
    // should put all processors into order and execute them
    order = [_.map(serviceConfig.processors, 'name')];
  }

  // run services layer by layer
  let layerResult = null;
  for (const serviceNames of order) {
    // extract service config
    const serviceConfigs = _.filter(serviceConfig.processors, s => serviceNames.indexOf(s.name) >= 0);
    // run layer services in parallel
    layerResult = await runServicesParallel(serviceConfigs, context);
    // update context
    _.extend(context, layerResult);
  }

  if (layerResult && _.values(layerResult).length > 0) {
    return _.values(layerResult)[0];
  }

  // return empty response
  return {};
}

/**
 * Build handlers for services.
 *
 * @param serviceConfig service configs.
 * @returns handlers for services
 */
function buildHandlers(serviceConfig) {
  const services = checkConfig(serviceConfig);
  const result = [];
  for (const service of services) {
    logger.info(`Building handlers for endpoint ${service.config.method.toUpperCase()} ${config.FACADE_SERVICE_API_PREFIX}${service.config.uri}`);

    // build handler. wrap service handler into an async function
    const handler = async function (request, response) {
      const finalResult = await serviceHandler(service.config, service.order, request);
      return finalResult;
    };
    result.push({
      path: `${config.FACADE_SERVICE_API_PREFIX}${service.config.uri}`,
      method: service.config.method,
      functionName: `${service.config.uri}`,
      handler,
      protected: service.config.protected
    });
  }
  return result;
}

export default buildHandlers;
