import _ from 'lodash';

/**
 * Get execution order of services.
 *
 * @param edges Edges of dependency graph. Service a depends on service b, then edge: b -> a.
 */
function serviceOrder(edges: string[][]) {
  const result = [];
  if (edges.length <= 0) {
    return result;
  }
  // get all nodes
  const nodes: string[] = [];
  _.each(edges, edge => {
    _.each(edge, node => {
      if (nodes.indexOf(node) < 0) {
        nodes.push(node);
      }
    });
  });

  // node count
  const n: number = nodes.length;

  // init array of to degree
  const toCount = {};
  _.each(nodes, node => {
    toCount[node] = 0;
  });
  _.each(edges, edge => {
    toCount[edge[1]] += 1;
  });

  // node count that has been calculated.
  let m = 0;
  // start iteration
  while (true) {
    // if all nodes are calculated, exit
    if (m === n) {
      break;
    }
    // find all nodes with to degree = 0
    const zeroNodes = [];
    _.each(_.keys(toCount), node => {
      if (toCount[node] === 0) {
        zeroNodes.push(node);
      }
    });

    // if there are no nodes found, circular dependency exists.
    if (zeroNodes.length <= 0) {
      throw new Error('Circular dependency exists or some service depends on unknown service.');
    }

    // put these nodes into result
    result.push(zeroNodes);
    m += zeroNodes.length;
    // clear these nodes
    _.each(zeroNodes, node => {
      delete toCount[node];
    });

    // update to degree
    _.each(edges, edge => {
      // if this edge starts from node
      if (zeroNodes.indexOf(edge[0]) >= 0) {
        const toNode = edge[1];
        // decrease to degree of toNode.
        if (toCount[toNode]) {
          toCount[toNode] -= 1;
        }
      }
    });
  }

  return result;
}

export default serviceOrder;

