import Axios from 'axios';
import _ from 'lodash';

import { PROCESSOR_TYPES } from '../constants';
import { logger } from '@pop-soda-facade/utils';
import { ParamHelper } from '@pop-soda-facade/facade-service/helper';

/**
 * Execute 'API' service.
 *
 * @param serviceConfig service config
 * @param context execution context
 * @returns response from api along with service name and type
 */
async function executeApi(serviceConfig, context) {
  const url = ParamHelper.extractParams(serviceConfig.uri, context);
  logger.info(`Starting API processor ${serviceConfig.name}: ${serviceConfig.request.method.toUpperCase()} ${url}`);

  // retrieve values from context
  const fields = ['header', 'params', 'body'];
  const parameters: any = ParamHelper.extractParams(_.pick(serviceConfig.request, ...fields), context);
  // construct request options
  const options: any = {
    url,
    method: serviceConfig.request.method,
    params: parameters.params,
    headers: parameters.header
  };
  if (String(serviceConfig.request.method).toLowerCase() !== 'get') {
    options.data = parameters.body;
  }
  logger.info(`with options: ${JSON.stringify(options)}`);
  return Axios.request(options).then((response) => {
    const responseData = {
      name: serviceConfig.name,
      type: serviceConfig.type,
      result: {
         body: response.data,
         headers: response.headers
      }
    };

    logger.info(`API Processor: ${serviceConfig.name} executed successfully with result: ${JSON.stringify(responseData.result)}`);
    return responseData;
  }).catch(err => {
    throw err;
  });
}

/**
 * Execute aggregator service.
 * @param serviceConfig service config
 * @param context execution context
 * @returns response from api along with service name and type
 */
async function executeAggregator(serviceConfig, context) {
  logger.info(`Starting transfomer processor: ${serviceConfig.name}`);

  // get its response and aggregate result
  const fields = ['header', 'body'];
  const result: any = ParamHelper.extractParams(_.pick(serviceConfig.response, ...fields), context);
  logger.info(`Transfomer processor: ${serviceConfig.name} executed successfully with result: ${JSON.stringify(result)}`);
  return { name: serviceConfig.name, type: serviceConfig.type, result };
}

/**
 * Execute a service.
 * @param serviceConfig service config
 * @param context execution context
 */
export default async function executeService(serviceConfig, context) {
  switch (serviceConfig.type) {
    case PROCESSOR_TYPES.API:
      return executeApi(serviceConfig, context);
    case PROCESSOR_TYPES.AGGREGATOR:
      return executeAggregator(serviceConfig, context);
    default:
      return Promise.reject(`Unknown service type ${serviceConfig.type}`);
  }
}
