import Hapi from '@hapi/hapi';
import path from 'path';

import { logger } from '@pop-soda-facade/utils';
import config from '@pop-soda-facade/app-config';

/**
 * Serve the static files
 * @param server Server Object
 */
function registerStaticServer (server) {
  server.route({
    method: 'GET',
    path: `/{param*}`,
    handler: {
      directory: {
        path: config.DIST_PATH,
        redirectToSlash: true,
        index: ['index.html']
      }
    }
  });

  logger.info(`Serving: / => ${config.DIST_PATH} [Domain: ${config.DOMAIN}]`);
}

/**
 * Export the plugin, to be consumed by the API.
 */
export const StaticFilePlugin = {
  name: 'StaticFilePlugin',
  register: (server: Hapi.Server) => {
    registerStaticServer(server);
  }
};