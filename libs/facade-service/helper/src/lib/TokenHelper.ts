import jwt from 'jsonwebtoken';
import stableStringify from 'json-stable-stringify';
import { v4 as uuid } from 'uuid';
import jwkToPem from 'jwk-to-pem';
import Boom from '@hapi/boom';
import config from '@pop-soda-facade/app-config';
import crypto from 'crypto';
import * as _ from 'lodash';
import anonymousTokenHelper from './AnonymousTokenHelper';
import AnonymousTokenHelper from './AnonymousTokenHelper';

/**
 * Generates the SHA256 fingerprint for the passed in string
 * @param {string} string
 */
function generateSHA256Fingerprint(string) {
  return crypto.createHash('sha256').update(string).digest('hex');
}


/**
 * Generates an anonymous token and returns it
 * @param clientId Client ID
 * @param publicKey Public Key (in JWK format)
 * @param resp The response object
 */
async function generateNewUserToken(clientId, domain, publicKey, req, resp) {
  const usn = uuid();
  const payload = { 
    iss: `${req.server.info.protocol}://${req.info.host}/`,
    aud: clientId, 
    cnf: {
      [domain]: publicKey
    },
    usn 
  };
  const token = anonymousTokenHelper.generateAnonymousToken(payload);
  resp.unstate('a_token');
  resp.state('a_token', token);
  return resp.response({ status: 'OK' });
}


/**
 * Refreshes a token
 * @param token The Token
 */
async function refreshToken(token: string) {
  // Set the key to be used for the token type
  const payloadKey = 'a_token';
  // Request a token refresh
  const payload: any = { [payloadKey]: token };
  return anonymousTokenHelper.refreshAnonymousToken(payload);
}

/**
 * Refreshes an expired token
 * @param token The Token
 * @param publicCert Public certificate used to sign the token
 */
async function verifyAndRefreshToken(token: string) {
  // Get the public certificate from the API server
  const publicCert = config.API_CERTIFICATE;
  try {
    // Try to decode the token
    let decodedToken = jwt.verify(token, publicCert);
    // Refresh token if needed
    if (decodedToken['exp'] - Math.floor(Date.now() / 1000) <= config.REFRESH_INTERVAL) {
      token = await refreshToken(token);
      decodedToken = jwt.verify(token, publicCert);
    }
    return { token, decodedToken };
    // If decode is successful, token doesn't need a refresh
  } catch (e) {
    if (e instanceof jwt.TokenExpiredError) {
     // Refresh the token if Anonymous token has expired
     const refreshedToken = await refreshToken(token);
     const decodedToken = jwt.verify(refreshedToken, publicCert);
     return { token: refreshedToken, decodedToken };
    }
  }
}

/**
 * Checks the nonce for the request
 * @param req Request object
 * @param jwk Public key in jwk format
 */
async function checkNonce(req, jwk) {
  // Extract the nonce from the request
  const nonce = req.headers['x-nonce'];
  // Convert it to the PEM format
  const pem = jwkToPem(jwk);
  // Use it to verify and decode the request's nonce
  const decodedNonce: any = jwt.verify(nonce, pem, { algorithms: ['ES256'] });
  // Generate the digest to calculate the nonce
  const fieldMap = { method: req.method, uri: req.url };
  let digest = '';
  for (const item of decodedNonce.ehts) {
    if (item === 'body') {
      const payload = stableStringify(req.payload);
      digest = `${digest}${payload}`;
    } else {
      digest = `${digest}${fieldMap[item]}`;
    }
  }
  // Calculate the SHA256 fingerprint of the digest to get the EDTS
  const edtsFingerprint = await generateSHA256Fingerprint(digest);
  // Check if the EDTS match
  if (edtsFingerprint !== decodedNonce.edts) {
    throw Boom.unauthorized('Invalid Nonce');
  }
}

/**
 * Checks if the passed token is an ID token
 * @param token The Token
 */
function isIdToken(token: string) {
  const decodedToken = jwt.decode(token);
  return _.has(decodedToken, 'sub');
}


/**
 * Returns an a_token from an id_token
 * @param idToken The ID Token
 */
async function anonymousTokenFromIdToken(usn: string, idToken: string) {
  if (!isIdToken(idToken)) {
    return idToken;
  }
  const decodedToken = jwt.decode(idToken);
  const payload = _.pick(decodedToken, ['usn', 'cnf', 'aud', 'iss']);
  if (usn) {
    payload['usn'] = usn;
  }
  const aToken = await AnonymousTokenHelper.generateAnonymousToken(payload);
  return aToken;
}

export default {
  verifyAndRefreshToken,
  generateNewUserToken,
  checkNonce,
  generateSHA256Fingerprint,
  isIdToken,
  anonymousTokenFromIdToken
};
