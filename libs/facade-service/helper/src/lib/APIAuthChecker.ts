import config from '@pop-soda-facade/app-config';
import jwt from 'jsonwebtoken';
import stableStringify from 'json-stable-stringify';
import _, { every, get, isNil } from 'lodash';
import jwkToPem from 'jwk-to-pem';
import { logger } from '@pop-soda-facade/utils';
import Boom from '@hapi/boom';
import TokenHelper from './TokenHelper';

/**
 * Checks whether an incoming request is authentic
 * @param req The incoming request
 * @param isPublic Whether the endpoint is public
 */
async function authenticateRequest(req, isPublic, res) {
  // Extract the token from the request
  let token = get(req, 'state.a_token') || get(req, 'state.id_token');
  logger.info('Authenticating the request');
  logger.debug('Auth headers: ' + JSON.stringify(req.headers));

  // Extract the nonce from the request
  const nonce = req.headers['x-nonce'];
  // Check whether token an nonce have sane values
  let checks = _.every({ token, nonce }, (item, key) => {
    const check = _.isString(item) && item.length > 0;
    if (!check) {
      logger.error(`Authentication failed: ${key} not found`);
    }
    return check;
  });

  // Checks failed. Handle it.
  if (!checks) return false;
  try {
    // Check validity
    const tokens = await TokenHelper.verifyAndRefreshToken(token);
    token = tokens.token;
    const decodedToken = tokens.decodedToken;
    // Get the domain of the server
    const domain = config.DOMAIN;
    // Sanity check for the client ID
    if (isNil(domain)) {
      throw Boom.badImplementation(`Server domain not found.`);
    }
    // Get the user's public certificate
    const jwk = get(decodedToken, ['cnf', domain]);
    // Convert it to the PEM format
    const pem = jwkToPem(jwk);
    // Use it to verify and decode the request's nonce
    const decodedNonce: any = jwt.verify(nonce, pem, { algorithms: ['ES256'] });
    // Generate the digest to calculate the nonce
    const fieldMap = { method: req.method, uri: req.url };
    let digest = '';
    for (const item of decodedNonce.ehts) {
      if (item === 'body') {
        continue;
      }
      digest = `${digest}${fieldMap[item]}`;
    }

    // If the request has a payload, we'll include that in the digest.
    if (req.method !== 'get' && req.payload) {
      const payload = stableStringify(req.payload);
      digest = `${digest}${payload}`;
    }
    logger.debug(`Generated digest: ${digest}`);
    // Calculate the SHA256 fingerprint of the digest to get the EDTS
    const edtsFingerprint = await TokenHelper.generateSHA256Fingerprint(digest);
    // Check if the EDTS match
    if (edtsFingerprint !== decodedNonce.edts) {
      logger.error('EDTS Mismatch!');
      return false;
    }
    // Request is valid, if it is for a public endpoint
    if (isPublic) {
      return true;
    }
    // Check if the ID Token has valid parameters
    checks = every([
      _.has(decodedToken, 'sub'),
      !isNil(decodedToken['sub']),
      decodedToken['sub'] !== ''
    ]);
    // Return the result
    return checks;
  } catch (e) {
    logger.error(e);
    return false;
  }
}

export default {
  authenticateRequest
};
