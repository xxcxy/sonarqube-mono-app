import _ from 'lodash';

/**
 * Fill parameters into val. Can be a string, number, array or object.
 *
 * @param val Value to fill
 * @param context execution context
 * @returns value that filled with context
 */
function extractParams(val, context) {
  if (_.isNil(val)) {
    return null;
  } else if (_.isNumber(val)) {
    return val;
  } else if (_.isString(val)) {
    if (val.startsWith('<%=') && val.endsWith('%>')) {
      // Replace <%=...%> with the JSON value from context
      const toBeReplaced = val.substring('<%='.length, val.length - '%>'.length).trim();
      return _.get(context, toBeReplaced, val);
    } else if (val.indexOf('<%') >= 0) {
      // Replace <%=...%>, <%...%> with the string value from context
      return _.template(val)(context);
    } else {
      return val;
    }
  } else if (_.isArray(val)) {
    return _.map(val, value => extractParams(value, context));
  } else if (_.isPlainObject(val)) {
    const res: any = {};
    _.forIn(val, (value, key) => {
      res[key] = extractParams(value, context);
    });
    return res;
  }
  // Technically it will never be here.
  return val;
}

export default {
  extractParams
};
