import Boom from '@hapi/boom';
import jwt from 'jsonwebtoken';

import * as _ from 'lodash';
import { includes } from 'lodash';
import { v4 as uuid } from 'uuid';
import config from '@pop-soda-facade/app-config';
import { logger } from "@pop-soda-facade/utils";
import { encrypt } from './crypto';

/**
 * Generates an ID token
 * @param req Request
 */
async function generateIdToken (payload) {
  // Extract JWT fields from the payload
  const { aud, cnf, iss, username } = payload;

  // Extract client ID from payload
  const clientId = aud;

  // Make sure client ID is valid
  if (!includes(config.ALLOWED_CLIENT_IDS, clientId)) {
    throw Boom.badImplementation(`Invalid client ID: ${clientId}`);
  }
  
  // Existence checks on the various payload items
  const checks = _.every({ aud, cnf, iss, username }, (item, key) => {
    const check = !_.isNaN(item);
    if (!check) {
      logger.error(`ID Token generation fail: ${key} not found`);
    }
    return check;
  });

  // Check(s) failed. Handle it.
  if (!checks) throw Boom.badRequest();

  // Generate the JWT payload for the ID token
  const usn = uuid();

  const authcode = encrypt(username);

  const jwtPayload = {
    usn,
    aud,
    authcode,
    cnf,
    sub: username,
    iss,
    iat: Math.floor(Date.now() / 1000),
    exp: Math.floor(Date.now() / 1000) + config.TOKEN_EXP_INTERVAL
  };

  // Sign the ID token
  const id_token = jwt.sign(jwtPayload, config.API_KEY, { algorithm: "RS256" });

  // Return the ID token and the updated anonymous token
  return id_token;
}

export default {
  generateIdToken
};