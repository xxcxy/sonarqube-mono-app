import jwt from 'jsonwebtoken';
import { set, includes } from 'lodash';
import Boom from '@hapi/boom';
import config from '@pop-soda-facade/app-config';

/**
 * Generates an anonymous token
 * @param payload the payload
 */
async function generateAnonymousToken(payload) {
    // Extract public key and client ID from payload
    const { aud, cnf, usn, iss } = payload;

    // Make sure client ID is valid
    if (!includes(config.ALLOWED_CLIENT_IDS, aud)) {
        throw Boom.badImplementation(`Invalid client ID: ${aud}`);
    }

    // Generate JWT payload
    const jwtPayload = {
        aud,
        usn,
        cnf,
        iss,
        exp: Math.floor(Date.now() / 1000) + config.TOKEN_EXP_INTERVAL,
    };

    // Sign the payload
    const a_token = jwt.sign(jwtPayload, config.API_KEY, { algorithm: "RS256" });

    // Return the anonymous token
    return a_token;
}

/**
 * Appends CNF for a new client ID to the anonymous token
 * @param payload the payload
 */
async function appendAnonymousToken(payload) {
    // Extract public key, client ID and existing anonymous token from request
    const { a_token, aud, cnf } = payload;

    // Make sure client ID is valid
    if (!includes(config.ALLOWED_CLIENT_IDS, aud)) {
        throw Boom.badImplementation(`Invalid client ID: ${aud}`);
    }
    
    // Decode and verify the existing anonymous token
    const data: any = jwt.verify(a_token, config.API_CERTIFICATE, { algorithms: [ 'RS256' ]});

    // update cnf
    set(data, 'cnf', cnf);

    // Extend expiry
    set(data, `exp`, Math.floor(Date.now() / 1000) + config.TOKEN_EXP_INTERVAL);

    // Sign the payload and generate JWT
    const newToken = jwt.sign(data, config.API_KEY, { algorithm: "RS256" });

    // Return the anonymous token
    return newToken;
}

/**
 * Refreshes the anonymous token
 * @param payload the payload
 */
async function refreshAnonymousToken(payload) {
    // Extract public key, client ID and existing anonymous token from request
    const { a_token } = payload;

    // Decode and verify the existing anonymous token
    let data: any;
    try {
        data = jwt.decode(a_token);
    } catch (e) {
        if (!(e instanceof jwt.TokenExpiredError)) {
            throw e;
        }
    }

    // Set exp to 24 hours from now
    set(data, `exp`, Math.floor(Date.now() / 1000) + config.TOKEN_EXP_INTERVAL);

    // Sign the payload and generate JWT
    const newToken = jwt.sign(data, config.API_KEY, { algorithm: "RS256" });

    // Return the anonymous token
    return newToken;
}

export default {
    generateAnonymousToken,
    appendAnonymousToken,
    refreshAnonymousToken
};