module.exports = {
  name: "postman-ui-lib-routes",
  preset: "../../../jest.config.js",
  coverageDirectory: "../../../coverage/libs/postman-ui-lib/routes",
  snapshotSerializers: [
    "jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js",
    "jest-preset-angular/build/AngularSnapshotSerializer.js",
    "jest-preset-angular/build/HTMLCommentSerializer.js"
  ]
};
