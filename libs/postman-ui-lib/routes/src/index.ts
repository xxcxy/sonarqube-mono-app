import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  {
    path: '',
    pathMatch:'full',
    redirectTo: '/dashboard'
  },
  {
    path: 'dashboard',
    loadChildren: () => import('@pop-soda-facade/postman-ui-lib/postman').then(m => m.UiLibPostmanModule)
  },
  {
    path: '**',
    redirectTo: '/dashboard'
  }
];
