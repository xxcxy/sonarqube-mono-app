import { NgModule, InjectionToken } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DataHelpersService } from './data-helpers/data-helpers.service';
import { HttpHelperService } from './http-helper/http-helper.service';
import { JwtService } from './jwt/jwt.service';
import { KeyService } from './key/key.service';
import { LoggerService } from './logger/logger.service';
import { StartupService } from './startup/startup.service';
import { LoginService } from './login/login.service';

const FACADE_API_PREFIX = new InjectionToken<string>('facade.api.prefix');

const services = [
  DataHelpersService,
  HttpHelperService,
  JwtService,
  KeyService,
  LoggerService,
  StartupService,
  LoginService
];

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [ ...services ],
  exports: []
})
export class UiLibServicesModule {}

export {
  DataHelpersService,
  HttpHelperService,
  JwtService,
  KeyService,
  LoggerService,
  StartupService,
  LoginService,
  FACADE_API_PREFIX
};