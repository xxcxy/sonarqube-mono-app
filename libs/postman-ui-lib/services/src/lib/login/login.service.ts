import { Injectable, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { LoggerService } from '../logger/logger.service';
import { HttpHelperService } from '../http-helper/http-helper.service';
import { BehaviorSubject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public loginObservable = new BehaviorSubject<boolean>(false);

  constructor(
    private logger: LoggerService,
    private httpHelper: HttpHelperService,
    private location: Location,
    private cookieService: CookieService,
    private router: Router
  ) {
    const isLoggedIn = !!(this.cookieService.get('id_token'));
    this.loginObservable.next(isLoggedIn);
  }

  /**
   * Returns true if user is logged in, false otherwise.
   */
  isAuthenticated() {
    return !!(this.cookieService.get('id_token'));
  }

  /**
   * Helper method to attempt a login.
   * @param username Username
   * @param password Password
   */
  async login(username, password) {
    if (!username || !password) {
      this.logger.log('Username and password are mandatory');
      return;
    }
    const url = this.location.prepareExternalUrl('api/login');
    const payload = { username, password };
    this.logger.log(`Requesting login: ${url}`);
    const response: any = await this.httpHelper.post(url, payload);
    this.loginObservable.next(true);
    return response;
  }

  /**
   * Log out.
   */
  async logout() {
    const url = this.location.prepareExternalUrl('api/logout');
    await this.httpHelper.post(url, {});
    this.router.navigate(['publicView']);
    this.loginObservable.next(false);
  }
}
