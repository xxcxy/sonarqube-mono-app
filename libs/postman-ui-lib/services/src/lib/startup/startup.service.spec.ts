import { TestBed } from '@angular/core/testing';

import { StartupService } from './startup.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { APP_BASE_HREF } from '@angular/common';


describe('StartupService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        CookieService,
        { provide: APP_BASE_HREF, useValue: '/my/app' }
      ]
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    const service: StartupService = TestBed.inject(StartupService);
    expect(service).toBeTruthy();
  });
});
