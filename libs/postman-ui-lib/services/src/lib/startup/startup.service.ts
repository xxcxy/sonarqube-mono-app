import { Injectable, Inject } from '@angular/core';
import { HttpHelperService } from '../http-helper/http-helper.service';
import { BehaviorSubject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import jwtDecode from 'jwt-decode';
import { APP_BASE_HREF } from '@angular/common';
import { get } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class StartupService {

  startupData: BehaviorSubject<boolean> = new BehaviorSubject(false);

  domainMap = {
    '/': 'localhost'
  };
  
  constructor(
    private httpHelper: HttpHelperService,
    private cookieService: CookieService,
    @Inject(APP_BASE_HREF) public baseHref: string,
  ) {}

  // Inspiration: https://gist.github.com/jed/982883
  generateUUID() {
    const templateString = "" + 1e7 + -1e3 + -4e3 + -8e3 + -1e11;
    // tslint:disable-next-line: no-bitwise
    const replaceFunction = () => (0 | Math.random() * 16).toString(16);
    return templateString.replace(/1|0/g, replaceFunction);
  }

  load(): Promise<any> {
    let wid;
    try {
      wid = JSON.parse(window.name || '');
      wid = get(wid, 'tmo-wid') || this.generateUUID();
    } catch {
      wid = this.generateUUID();
    }
    window.name = JSON.stringify({ 'tmo-wid': wid });
    let a_token = this.cookieService.get('a_token');
    if (a_token) {
      a_token = jwtDecode(a_token) || {};
      const cnfKey = get(a_token, ['cnf', this.domainMap[this.baseHref]]);
      if (a_token && cnfKey) {
        this.startupData.next(true);
        return Promise.resolve();
      }
    }

    return this.httpHelper.getAnonymousToken()
      .then(() => this.startupData.next(true))
      .catch(() => this.startupData.next(true));
  }
}
