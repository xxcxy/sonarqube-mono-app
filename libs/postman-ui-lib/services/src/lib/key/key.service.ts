import { Injectable } from '@angular/core';
import { DataHelpersService } from '../data-helpers/data-helpers.service';
import { isNil } from 'lodash';
import { set, get } from 'idb-keyval';

@Injectable({
  providedIn: 'root'
})
export class KeyService {

  constructor(
    private dataHelper: DataHelpersService
  ) { }

  /**
   * Exports a CryptoKey into a jwk
   * @param keys A key set (only publicKey is needed)
   */
  async exportKey(keys) {
    const jwk = await window.crypto.subtle.exportKey('jwk', keys.publicKey);
    return jwk;
  }

  /**
   * Gets the key pair
   */
  async getKey(): Promise<CryptoKeyPair> {
    const key: CryptoKeyPair = await get(1);
    if (!isNil(key)) {
      return key;
    }
    const keyType = {
      name: 'ECDSA',
      namedCurve: 'P-256',
      hash: { name: 'SHA-256' }
    };
    const privileges = ['sign', 'verify'];
    const keys = await window.crypto.subtle.generateKey(keyType, false, privileges);
    set(1, keys);
    return keys;
  }

  /**
   * Generates the thumbprint of a JWK
   * Used to generate a JWT
   * @param jwk JWK
   */
  async thumbprint(jwk) {
    const sortedPub = '{"crv":"CRV","kty":"EC","x":"X","y":"Y"}'
      .replace('CRV', jwk.crv)
      .replace('X', jwk.x)
      .replace('Y', jwk.y);

    const fingerprint = await window.crypto.subtle.digest(
      { name: 'SHA-256' },
      this.dataHelper.strToUint8(sortedPub)
    );
    return this.dataHelper.uint8ToUrlBase64(new Uint8Array(fingerprint));
  }

  /**
   * Signs some data and returns the signature.
   * @param data Data
   */
  async sign (data) {
    const keys: CryptoKeyPair = await get(1);
    const sigType = { name: 'ECDSA', hash: { name: 'SHA-256' } };
    return window.crypto.subtle.sign(sigType, keys.privateKey, data);
  }
}
