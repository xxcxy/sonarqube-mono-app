import { async, TestBed } from "@angular/core/testing";
import { UiLibServicesModule } from "@pop-soda-facade/postman-ui-lib/services";

describe("UiLibServicesModule", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UiLibServicesModule]
    }).compileComponents();
  }));

  it("should create", () => {
    expect(UiLibServicesModule).toBeDefined();
  });
});
