import { Injectable } from '@angular/core';
import { isFunction } from 'util';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  private loggerFn: Function = console.log;

  constructor() { }

  /**
   * Sets the logger function to be used by the various services of the app
   * @param loggerFn A function which accepts a string
   */
  setLogger(loggerFn: Function) {
    this.loggerFn = loggerFn;
  }

  /**
   * Add some message to the current log
   * @param msg Message
   */
  log(msg) {
    if (isFunction(this.loggerFn)) {
      this.loggerFn(msg);
    }
  }
}
