import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { LoggerService } from '../logger/logger.service';
import { merge, once, forEach } from 'lodash';
import { JwtService } from '../jwt/jwt.service';
import { KeyService } from '../key/key.service';
import { CookieService } from 'ngx-cookie-service';
import stableStringify from 'json-stable-stringify';

type HttpMethods = 'get' | 'post' | 'put' | 'patch' | 'delete';

@Injectable({
  providedIn: 'root'
})
export class HttpHelperService {

  constructor(
    private http: HttpClient,
    private logger: LoggerService,
    private jwt: JwtService,
    private keyService: KeyService,
    private location: Location,
    private cookieService: CookieService
  ) { }

  // Wrapper over _getAnonymousTokenFunction, to make sure it is only called once.
  private _getAnonymousToken = once(this._getAnonymousTokenFunction);

  /**
   * Generates an anonymous token.
   * Called once, every time during app startup.
   */
  private async _getAnonymousTokenFunction () {
    this.logger.log(`Checking if keys exist... `);
    const keys = await this.keyService.getKey();
    this.logger.log(`Private key: ${keys.privateKey}`);
    const jwkKey = await this.keyService.exportKey(keys);
    this.logger.log(`Public key: ${JSON.stringify(jwkKey, null, 2)}`);
    let url = this.location.prepareExternalUrl('/api/auth');
    const data = { 'publicKey': jwkKey };
    url = `${url}${window.location.search}`;
    const res = await this.post(url, data);
    return new Promise((resolve) => {
      if (res.body.redirect) {
        window.location.href = res.body.redirect;
      } else {
        resolve(res);
      }
    });
  }

  /**
   * Wrapper for _getAnonymousToken().
   * Required to make sure it is only called once.
   */
  public async getAnonymousToken(overrideOnce = false) {
    if (overrideOnce) {
      return this._getAnonymousToken();
    }
    return this._getAnonymousTokenFunction();
  }

  /**
   * Generates SHA256 Fingerprint for a given string.
   * @param string String
   */
  async generateSHA256Fingerprint(string) {
    const msgUint8 = new TextEncoder().encode(string);
    const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
    return hashHex;
  }

  /**
   * Generates a nonce for an HTTP request, given it's method, URL and body.
   * @param method Request Method
   * @param url Request URL
   * @param body Request Body
   */
  async generateNonce(method: HttpMethods, url: string, params: any, body: any = {}) {
    const ehts = ['method', 'uri'];
    let edts = `${method}${url}`;
    if (params) {
      let queryParams = '';
      forEach(params, (value, key) => {
        queryParams += queryParams === '' ? '?' : '&';
        queryParams += `${key}=${encodeURIComponent(value)}`;
      });
      edts = `${edts}${queryParams}`;
    }
    if (method !== 'get') {
      ehts.push('body');
      edts = `${edts}${stableStringify(body)}`;
    }
    this.logger.log(`ehts: ${JSON.stringify(ehts, null, 2)}`);
    this.logger.log(`edts string: ${edts}`);
    const edtsFingerprint = await this.generateSHA256Fingerprint(edts);
    this.logger.log(`edts fingerprint: ${edtsFingerprint}`);
    let nonce: any = { ehts, edts: edtsFingerprint };
    this.logger.log(`Nonce: ${JSON.stringify(nonce)}`);
    nonce = await this.jwt.generateJWT(nonce);
    return nonce;
  }

  /**
   * Helper method to create a GET request to a server using POP JWT authentication
   * @param url URL
   * @param options Request options
   */
  async get(url, options : any = {}) {
    const fullUrl = `${window.location.origin}${url}`;
    const nonce = await this.generateNonce('get', fullUrl, options.params);
    const wid = JSON.parse(window.name)['tmo-wid'];
    this.logger.log(`Options: ${JSON.stringify(options)}`);
    merge(options, { headers: { 'x-nonce': nonce, 'x-wid': wid }, observe: 'response', responseType: 'text' });
    this.logger.log(`GET ${url}: ${JSON.stringify(options)}`);
    const res: any = await this.http.get(url, options).toPromise();
    this.logger.log(`Response: ${JSON.stringify(res.body, null, 2)}`);
    this.logger.log(`Headers: ${JSON.stringify(res.headers.normalizedNames, null, 2)}`);
    this.logger.log(`Cookies: ${document.cookie}`);
    return res;
  }
  /**
   * Helper method to create a POST request to a server using POP JWT authentication
   * @param url URL
   * @param payload Request body
   * @param options Request options
   */
  async post(url, payload, options: any = {}) {
    const fullUrl = `${window.location.origin}${url}`;
    const nonce = await this.generateNonce('post', fullUrl, options.params, payload);
    this.logger.log(`Nonce: ${nonce}`);
    const wid = JSON.parse(window.name)['tmo-wid'];
    this.logger.log(`Options: ${JSON.stringify(options)}`);
    merge(options, { headers: { 'x-nonce': nonce, 'x-wid': wid }, observe: 'response' });
    this.logger.log(`POST ${url}: ${JSON.stringify(payload)} : ${JSON.stringify(options)}`);
    const res: any = await this.http.post(url, payload, options).toPromise();
    this.logger.log(`Response: ${JSON.stringify(res.body, null, 2)}`);
    this.logger.log(`Headers: ${JSON.stringify(res.headers.normalizedNames, null, 2)}`);
    this.logger.log(`Cookies: ${document.cookie}`);
    return res;
  }

  /**
   * Helper method to create a request to a server using POP JWT authentication
   * @param url URL
   * @param payload Request body
   * @param options Request options
   */
  async request(method, url, payload, options: any = {}) {
    const fullUrl = `${window.location.origin}${url}`;
    const nonce = await this.generateNonce(method, fullUrl, options.params, payload);
    this.logger.log(`Nonce: ${nonce}`);
    const wid = JSON.parse(window.name)['tmo-wid'];
    this.logger.log(`Options: ${JSON.stringify(options)}`);
    //merge(options.header, { headers: { 'x-nonce': nonce, 'x-wid': wid }, observe: 'response' });
    merge(options, { headers: { 'x-nonce': nonce, 'x-wid': wid }, observe: 'response' });
    this.logger.log(`${method.toUpperCase()} ${url}: ${JSON.stringify(payload)} : ${JSON.stringify(options)}`);
    if (payload) {
      options.body = payload;
    }
    const res: any = await this.http.request(method, url, options).toPromise();
    this.logger.log(`Response: ${JSON.stringify(res.body, null, 2)}`);
    this.logger.log(`Headers: ${JSON.stringify(res.headers.normalizedNames, null, 2)}`);
    this.logger.log(`Cookies: ${document.cookie}`);
    return res;
  }
}
