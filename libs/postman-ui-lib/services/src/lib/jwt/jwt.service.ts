import { Injectable } from '@angular/core';
import { DataHelpersService } from '../data-helpers/data-helpers.service';
import { KeyService } from '../key/key.service';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor(
    private dataHelper: DataHelpersService,
    private keyService: KeyService
  ) { }

  /**
   * Generates a JWT with the provided claims
   * @param claims Claims
   */
  async generateJWT(claims) {
    const keys = await this.keyService.getKey();
    const jwk = await this.keyService.exportKey(keys);

    const headers = {
      typ: 'JWT',
      alg: 'ES256',
      jwk: { kty: jwk.kty, crv: jwk.crv, x: jwk.x, y: jwk.y },
      kid: await this.keyService.thumbprint(jwk)
    };

    const protectedHeaders = this.dataHelper.strToUrlBase64(JSON.stringify(headers));
    const payload = this.dataHelper.strToUrlBase64(JSON.stringify(claims));

    // Convert UTF-8 to Uint8Array ArrayBuffer
    const data = this.dataHelper.strToUint8(`${protectedHeaders}.${payload}`);

    // returns an ArrayBuffer containing a JOSE (not X509) signature,
    let signature: String | ArrayBuffer = await this.keyService.sign(data);

    // which must be converted to Uint8 to be useful
    signature = this.dataHelper.uint8ToUrlBase64(new Uint8Array(signature));

    return `${protectedHeaders}.${payload}.${signature}`;
  }
}
