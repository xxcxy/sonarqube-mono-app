import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataHelpersService {
  
  constructor() { }

  /**
  * String to UInt8 Array
  * @param str String
  */
  strToUint8(str) {
    return new TextEncoder().encode(str);
  }

  /**
   * UCS-2 String to URL-Safe Base64
   * @param str String
   */
  strToUrlBase64(str) {
    return this.binToUrlBase64(this.utf8ToBinaryString(str));
  }

  /**
   * Binary String to URL-Safe Base64
   * @param bin Binary String
   */
  binToUrlBase64(bin) {
    return btoa(bin)
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=+/g, '');
  }

  /**
   * UTF-8 to Binary String
   * @param str String
   */
  utf8ToBinaryString(str) {
    const escStr = encodeURIComponent(str);
    // replaces any uri escape sequence, such as %0A,
    // with binary escape, such as 0x0A
    const binStr = escStr.replace(/%([0-9A-F]{2})/g, (match, p1) => {
      return String.fromCharCode(parseInt(p1, 16));
    });
    
    return binStr;
  }
  
  /**
   * Uint8Array to URL Safe Base64
   * @param uint8 UInt8 Array
   */
  uint8ToUrlBase64(uint8) {
    let bin = '';
    uint8.forEach( (code) => {
      bin += String.fromCharCode(code);
    });
    return this.binToUrlBase64(bin);
  }
  
}
