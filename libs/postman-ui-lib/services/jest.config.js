module.exports = {
  name: "postman-ui-libn-ui-lib-services",
  preset: "../../../jest.config.js",
  coverageDirectory: "../../../coverage/libs/postman-ui-libn-ui-lib/services",
  snapshotSerializers: [
    "jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js",
    "jest-preset-angular/build/AngularSnapshotSerializer.js",
    "jest-preset-angular/build/HTMLCommentSerializer.js"
  ]
};
