import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable, Subscription, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { ApiCollection } from './models/collection';
import { ApiRequest } from './models/api-request';
import * as collectionActions from './store/actions/collection';
import * as postmanStore from './store';
import { LoginDialogComponent } from './components/login-dialog/login-dialog.component';
import { LoginService } from '@pop-soda-facade/postman-ui-lib/services';


@Component({
  selector: 'ui-postman',
  templateUrl: './postman.component.html',
  styleUrls: ['./postman.component.scss']
})
export class PostmanComponent implements OnInit, OnDestroy {

  @ViewChild('fileInput', { static: true }) fileInput: ElementRef<HTMLInputElement>;

  public isLoggedIn = false;

  subscriptions = new Subscription();

  collection$: Observable<ApiCollection> = this.store.select(postmanStore.getCollection);
  totalRequests$: Observable<number> = this.store.select(postmanStore.totalRequests);
  isCollectionDirty$: Observable<boolean> = this.store.select(postmanStore.isCollectionDirty);
  errorText$: Observable<string> = this.store.select(postmanStore.errorText);

  selectedRequest$: Observable<ApiRequest> = this.store.select(postmanStore.getSelectedRequest);
  isSelectedRequestDirty$: Observable<boolean> = this.store.select(postmanStore.isRequestDirty);
  isSendingRequest$: Observable<boolean> = this.store.select(postmanStore.isSendingRequest);

  collectionIsDirty = false;
  collectionIsLoaded = false;

  constructor(
    private store: Store<postmanStore.PostmanState>,
    private dialogService: MatDialog,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    const sub1 = this.isCollectionDirty$.subscribe((isDirty: boolean) => this.collectionIsDirty = isDirty);
    this.subscriptions.add(sub1);

    const sub2 = this.collection$.subscribe(value => this.collectionIsLoaded = !!value);
    this.subscriptions.add(sub2);

    const sub3 = this.loginService.loginObservable.subscribe(value => this.isLoggedIn = value);
    this.subscriptions.add(sub3);

    this.store.dispatch(collectionActions.loadFromStorage());
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  // create a new empty collection
  createNewCollection() {
    const confirmation$: Observable<boolean> = this.collectionIsLoaded ? this.showConfirmationDialog() : of(true);
    confirmation$.subscribe(result => result && this.store.dispatch(collectionActions.createNew()));
  }

  // import collection
  importCollection() {
    const confirmation$: Observable<boolean> = this.collectionIsLoaded ? this.showConfirmationDialog() : of(true);
    confirmation$.subscribe(result => result && this.fileInput.nativeElement.click());
  }

  importFileSelected(e: Event) {
    const file = this.fileInput.nativeElement.files[0];
    this.fileInput.nativeElement.value = null;
    this.store.dispatch(collectionActions.importCollection({ file }));
  }

  // export collection
  exportCollection() {
    this.store.dispatch(collectionActions.exportCollection());
  }

  /**
   * before creating or importing a new collection, 
   * show a confirmation dialog if there is already a collection
   * also checks if there are unsaved changes and changes the text accordingly
   */
  showConfirmationDialog() {
    const dialogRef = this.dialogService.open(ConfirmationDialogComponent, {
      width: '350px',
      data: {
        headerText: this.collectionIsDirty ? 'Changes Not Exported' : 'A Collection is Active',
        confirmText: this.collectionIsDirty ?
          'There are unsaved changes that will be lost if you create a new API. Are you sure?'
          : 'The existing collection will be overridden. Are you sure? ',
      }
    });

    return dialogRef.afterClosed().pipe(
      map(result => result === 'cancel' ? false : true)
    );
  }

  openLoginDialog() {
    const dialogRef = this.dialogService.open(LoginDialogComponent, {
      width: '500px'
    });

    return dialogRef.afterClosed().pipe(
      map(result => result === 'cancel' ? false : true)
    );
  }

  async logout() {
    await this.loginService.logout();
  }

}
