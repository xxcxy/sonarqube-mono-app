import { async, TestBed } from "@angular/core/testing";
import { UiLibPostmanModule } from "./ui-lib-postman.module";

describe("UiLibPostmanModule", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UiLibPostmanModule]
    }).compileComponents();
  }));

  it("should create", () => {
    expect(UiLibPostmanModule).toBeDefined();
  });
});
