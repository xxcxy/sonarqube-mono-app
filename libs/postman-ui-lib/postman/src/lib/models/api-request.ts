import { KeyValueData } from './collection';

export enum RequestMethods {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
  PATCH = 'PATCH'
}

export enum RequestBodyModes {
  JSON = 'json',
  FORMDATA = 'form-data',
  URLENCODED = 'urlencoded'
}

export type RequestBody =
{ mode: RequestBodyModes.JSON; jsonString: string; } |
{ mode: RequestBodyModes.FORMDATA; formdata: KeyValueData[]; } |
{ mode: RequestBodyModes.URLENCODED; urlencoded: KeyValueData[]; };

export interface Request {
  method: RequestMethods;
  url: string;
  headers?: KeyValueData[];
  body?: RequestBody;
  params?: KeyValueData[];
}

export interface Response {
  status: number;
  time: number;
  size: number;
  body: string;
  headers: KeyValueData[];
  cookies: {
    name: string;
    value: string;
    domain: string;
    path: string;
    expires: string | number;
  }[];
}

export interface TestsData {
  variable: string;
  responsePath: string;
}

export interface ApiRequest {
  id: string;
  name: string;
  description?: string;
  tests?: TestsData[];
  request: Request;
  response?: Response;
}