import { ApiRequest } from './api-request'; 

export interface CollectionItem {
  id: string;
  name: string;
  item: Array<CollectionItem | ApiRequest>;
}

export interface ApiCollection extends CollectionItem {
  envVariables: KeyValueData[];
}

export interface KeyValueData {
  key: string;
  value: string;
  description?: string;
  disabled?: boolean;
}