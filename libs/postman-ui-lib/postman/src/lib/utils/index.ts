import { forEach, replace } from 'lodash';

// returns true if the passed string can be successfuly parsed as json
export function isJsonValid(str: string): boolean {
  try {
    JSON.parse(str);
    return true;
  } catch (e) {
    return false;
  }
}

export function parseAndReplace(str: string, variables: { key: string, value: string }[]): string {
  forEach(variables, ({ key, value}) => {
    str = replace(str, new RegExp(`{{${key}}}`, 'g'), value);
  });
  return str;
}