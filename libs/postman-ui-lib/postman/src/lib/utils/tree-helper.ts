// collection tree helper functions
import { v4 as uuidv4 } from 'uuid';
import { reduce, every, omit } from 'lodash';

import { CollectionItem } from '../models/collection';
import { ApiRequest } from '../models/api-request';


// util function to parse collection json  object
// add ids to each node ,recursively
export function parseJsonObject(obj: any, stripResponse = true) {
  if (stripResponse) {
    obj = omit(obj, 'response');
  }

  const parsedObj: any = {
    ...obj,
    id: uuidv4()
  };
  if ('item' in obj) {
    parsedObj.item = reduce(obj.item, (accu, item) => {
      return accu.concat(parseJsonObject(item));
    }, []);
  }

  return parsedObj;
}

// util function to parse collection object to json for export
// strip all ids and the response object
export function parseCollection(obj: any) {
  const parsedObj: any = omit(obj, 'id', 'response');

  if ('item' in obj) {
    parsedObj.item = reduce(obj.item, (accu, item) => {
      return accu.concat(parseCollection(item));
    }, []);
  }

  return parsedObj;
}

export function getNodeById(object: CollectionItem | ApiRequest, nodeId: string) {
  if (object.id === nodeId) {
    return object;
  } else {
    if ('item' in object) {
      let matchedObj;
      every(object.item, obj => {
        matchedObj = getNodeById(obj, nodeId);
        return !matchedObj;
      });
      return matchedObj;
    } else {
      return false;
    }
  }
}

export function getParentNodeById(object: CollectionItem | ApiRequest, nodeId: string, parent: CollectionItem = null) {
  if (object.id === nodeId) {
    return parent;
  } else {
    if ('item' in object) {
      let parentObj;
      every(object.item, obj => {
        parentObj = getParentNodeById(obj, nodeId, object);
        return !parentObj;
      });
      return parentObj;
    } else {
      return false;
    }
  }
}