import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTreeModule } from '@angular/material/tree';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';

const MaterialComponents = [
  FormsModule,
  ClipboardModule,
  MatToolbarModule,
  MatButtonModule,
  MatIconModule,
  MatDividerModule,
  MatDialogModule,
  MatTreeModule,
  MatMenuModule,
  MatSelectModule,
  MatTabsModule,
  MatCheckboxModule,
  MatRadioModule,
  MatButtonToggleModule,
  MatSnackBarModule,
  MatInputModule
];


@NgModule({
  imports: [
    ...MaterialComponents
  ],
  exports: [
    ...MaterialComponents
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        restoreFocus: false,
        disableClose: true,
        hasBackdrop: true,
        maxWidth: '80vw',
        maxHeight: '80vh'
      }
    }
  ]
})

export class MaterialComponentsModule { }
