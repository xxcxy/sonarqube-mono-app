import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, exhaustMap, withLatestFrom, tap, filter as rxjsFilter } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { saveAs } from 'file-saver';
import { filter, findIndex } from 'lodash';

import { parseJsonObject, getNodeById, getParentNodeById, parseCollection } from '../../utils/tree-helper';
import { ApiRequest, RequestMethods } from '../../models/api-request';
import { ApiCollection, CollectionItem } from '../../models/collection';
import * as postmanStore from '../index';
import {
  CollectionActionTypes,
  loadCollection,
  importFileError,
  updateDraftCollection
} from '../actions/collection';
import { selectRequest } from '../actions/request-editor';
import { isJsonValid } from '../../utils';


const LocalStorageKey = 'ui_postman_collection';

@Injectable()
export class CollectionEffects {

  constructor(
    private actions$: Actions,
    private store: Store<postmanStore.PostmanState>
  ) {}

  loadFromStorage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CollectionActionTypes.LoadFromStorage),
      rxjsFilter(() => localStorage.getItem(LocalStorageKey) && isJsonValid(localStorage.getItem(LocalStorageKey))),
      map(() => {
        const collection: ApiCollection = JSON.parse(localStorage.getItem(LocalStorageKey));
        return loadCollection({ payload: collection });
      })
    )
  );

  createNew$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CollectionActionTypes.CreateNew),
      map(() => {
        const newCollection: ApiCollection = {
          id: uuidv4(),
          envVariables: [],
          name: 'Collection',
          item: []
        };

        return loadCollection({ payload: newCollection});
      })
    )
  );

  importCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CollectionActionTypes.ImportCollection),
      exhaustMap(({ file }: { file: File }) => {
        const errorText = 'Invalid JSON file.<br/>Check the console for more details.';

        const fileReader = new FileReader();
        fileReader.readAsText(file, "UTF-8");

        return new Observable<Action>(obs => {
          fileReader.onload = () => {
            try {
              const fileData = JSON.parse(fileReader.result as string);
              const collection: ApiCollection = parseJsonObject(fileData);
              obs.next(loadCollection({ payload: collection }));
              obs.complete();
            } catch (e) {
              obs.next(importFileError({ errorText }));
              obs.complete();
            }
          };
  
          fileReader.onerror = () => {
            obs.next(importFileError({ errorText }));
            obs.complete();
          };
        });
      })
    )
  );

  loadCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CollectionActionTypes.LoadCollection),
      map(() => selectRequest({ request: null }))
    )
  );

  saveToStorage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        CollectionActionTypes.LoadCollection,
        CollectionActionTypes.UpdateDraftCollection
      ),
      tap(({ payload }) => {
        localStorage.setItem(LocalStorageKey, JSON.stringify(payload));
      })
    ),
    { dispatch: false }
  );

  exportCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CollectionActionTypes.ExportCollection),
      withLatestFrom(this.store.select(postmanStore.getCollection)),
      map(([ _, draftCollection ]) => {
        const exportObj = parseCollection(draftCollection);
        const jsonString= JSON.stringify(exportObj, null, 2);
        const blob = new Blob([jsonString], { type: 'application/json' });

        saveAs(blob, `${draftCollection.name}.json`);
      })
    ), { dispatch: false }
  );

  saveNode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CollectionActionTypes.SaveNode),
      withLatestFrom(this.store.select(postmanStore.getCollection)),
      map(([{ node }, draftCollection]: [{ node: ApiRequest | CollectionItem }, ApiCollection]) => {
        // if its the root node
        if (draftCollection.id === node.id) {
          draftCollection.name = node.name;
        } else {
          const parentNode = getParentNodeById(draftCollection, node.id);

          if (parentNode && 'item' in parentNode) {
            const index = findIndex(parentNode.item, { id: node.id });
            parentNode.item[index] = node;
          }
        }

        return updateDraftCollection({ payload: draftCollection });
      })
    )
  );

  // add new folder to a particular folder with given id
  addFolder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CollectionActionTypes.AddFolder),
      withLatestFrom(this.store.select(postmanStore.getCollection)),
      map(([ { nodeId }, draftCollection ]) => {
        const newFolder: CollectionItem = {
          id: uuidv4(),
          name: 'New Folder',
          item: []
        };
        const nodeToUpdate = getNodeById(draftCollection, nodeId);
        if (nodeToUpdate && 'item' in nodeToUpdate) {
          nodeToUpdate.item.unshift(newFolder);
        }
        return updateDraftCollection({ payload: draftCollection });
      })
    )
  );

  // add new request to a particular folder with a given id
  addRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CollectionActionTypes.AddRequest),
      withLatestFrom(this.store.select(postmanStore.getCollection)),
      map(([ { nodeId }, draftCollection ]) => {
        const newRequest: ApiRequest = {
          id: uuidv4(),
          name: 'New Request',
          request: {
            method: RequestMethods.GET,
            url: ''
          }
        };

        const nodeToUpdate = getNodeById(draftCollection, nodeId);
        if (nodeToUpdate && 'item' in nodeToUpdate) {
          nodeToUpdate.item.push(newRequest);
        }

        return updateDraftCollection({ payload: draftCollection });
      })
    )
  );

  // duplicate a node with a given id, it could be both a folder or a request
  duplicateNode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CollectionActionTypes.DuplicateNode),
      withLatestFrom(this.store.select(postmanStore.getCollection)),
      map(([ { node }, draftCollection ]: [{ node: ApiRequest | CollectionItem }, ApiCollection]) => {
        const parentNode = getParentNodeById(draftCollection, node.id);
        // do this to put new ids to the duplicated nodes
        node = parseJsonObject(node);
        
        if (parentNode && 'item' in parentNode) {
          // add copy to the existing name
          node.name = `${node.name} (Copy)`;
          if ('item' in node) {
            parentNode.item.unshift(node);
          } else {
            parentNode.item.push(node);
          }
        }

        return updateDraftCollection({ payload: draftCollection });
      })
    )
  );

  deleteNode$ = createEffect(() => 
    this.actions$.pipe(
      ofType(CollectionActionTypes.DeleteNode),
      withLatestFrom(
        this.store.select(postmanStore.getCollection),
        this.store.select(postmanStore.getSelectedRequest)
      ),
      exhaustMap(([{ nodeId }, draftCollection, draftRequest]) => {
        const parentNode = getParentNodeById(draftCollection, nodeId);

        if (parentNode && 'item' in parentNode) {
          parentNode.item = filter(parentNode.item, item => item.id !== nodeId);
        }

        const actions: Action[] = [
          updateDraftCollection({ payload: draftCollection })
        ];

        if (draftRequest && draftRequest.id === nodeId) {
          actions.push(selectRequest({ request: null }));
        }

        return actions;
      })
    )
  );
}

