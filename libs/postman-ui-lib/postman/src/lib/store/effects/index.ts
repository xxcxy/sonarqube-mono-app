import { CollectionEffects } from './collection';
import { RequestEditorEffects } from './request-editor';

export default [
  CollectionEffects,
  RequestEditorEffects
];
