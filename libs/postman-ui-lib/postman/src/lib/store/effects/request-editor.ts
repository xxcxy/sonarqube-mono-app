import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store, Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, of } from 'rxjs';
import { map, switchMap, withLatestFrom, filter } from 'rxjs/operators';
import { findIndex, forEach, startsWith, replace, get, find } from 'lodash';

import { ConfirmationDialogComponent } from '../../components/confirmation-dialog/confirmation-dialog.component';
import * as postmanStore from '../index';
import { ApiRequest } from '../../models/api-request';
import { getParentNodeById } from '../../utils/tree-helper';
import { isJsonValid } from '../../utils';
import {
  RequestEditorActionTypes,
  selectRequest,
  saveRequest,
  saveResponse,
  executeRequestTests,
  cancelApiRequest
} from '../actions/request-editor';
import { updateDraftCollection, saveNode as saveCollectionNode } from '../actions/collection';
import { PostmanService } from '../../postman.service';


@Injectable()
export class RequestEditorEffects {

  constructor(
    private actions$: Actions,
    private store: Store<postmanStore.PostmanState>,
    private dialogService: MatDialog,
    private postmanService: PostmanService
  ) {}

  selectRequestInit$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RequestEditorActionTypes.SelectRequestInit),
      withLatestFrom(
        this.store.select(postmanStore.getSelectedRequest),
        this.store.select(postmanStore.isRequestDirty)
      ),
      switchMap(([{ node }, selectedRequest, isRequestDirty ]: [{ node: ApiRequest }, ApiRequest, boolean]) => {
        // if the passed node is already selected do nothing
        if (selectedRequest && node.id === selectedRequest.id) {
          return EMPTY;
        }

        if (isRequestDirty) {
          const dialogRef = this.dialogService.open(ConfirmationDialogComponent, {
            width: '450px',
            data: {
              headerText: 'Request Changes Not Saved',
              confirmText: 'There are unsaved changes in the request, that will be lost if you don\'t save them. Do you want to save?',
              confirmButtonText: 'Save Changes',
              discardButtonText: 'Discard Changes'
            }
          });

          return dialogRef.afterClosed().pipe(
            switchMap(response => {
              switch (response) {
                case 'cancel':
                  return EMPTY;

                case 'discard':
                  return of(selectRequest({ request: node }));

                case 'confirm':
                  return [
                    saveRequest(),
                    selectRequest({ request: node })
                  ];
              }
            })
          );
        } else {
          return of(selectRequest({ request: node }));
        }
      })
    )
  );

  saveRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RequestEditorActionTypes.SaveRequest),
      withLatestFrom(
        this.store.select(postmanStore.getCollection),
        this.store.select(postmanStore.getSelectedRequest)
      ),
      map(([_, draftCollection, draftRequest]) => {
        const parentNode = getParentNodeById(draftCollection, draftRequest.id);
        if (parentNode && 'item' in parentNode) {
          const index = findIndex(parentNode.item, { id: draftRequest.id });
          parentNode.item.splice(index, 1, draftRequest);
        }

        return updateDraftCollection({ payload: draftCollection });
      })
    )
  );

  sendApiRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        RequestEditorActionTypes.SendApiRequest,
        RequestEditorActionTypes.CancelApiRequest
      ),
      withLatestFrom(
        this.store.select(postmanStore.getCollection),
        this.store.select(postmanStore.getSelectedRequest),
        this.store.select(postmanStore.getOriginalRequest)
      ),
      switchMap(([action, draftCollection, draftRequest, originalRequest]) => {
        if ((action as Action).type === RequestEditorActionTypes.CancelApiRequest) {
          return EMPTY;
        }

        return this.postmanService.sendRequest(draftRequest.request, draftCollection.envVariables)
          .pipe(
            switchMap(response => {
              const actions: Action[] = [
                saveResponse({ response }),
                saveCollectionNode({ node: { ...originalRequest, response }})
              ];

              if (response.status === 200) {
                actions.push(executeRequestTests({ responseBody: response.body }));
              }

              return actions;
            })
          );
      })
    )
  );

  executeRequestTests$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RequestEditorActionTypes.ExecuteRequestTests),
      withLatestFrom(
        this.store.select(postmanStore.getCollection),
        this.store.select(postmanStore.getSelectedRequest)
      ),
      filter(([_, __, draftRequest]) => draftRequest.tests && draftRequest.tests.length > 0),
      map(([{ responseBody }, draftCollection, draftRequest]) => {
        const isResponseBodyText = !isJsonValid(responseBody);
        const envVariables = draftCollection.envVariables || [];

        forEach(draftRequest.tests, ({ variable, responsePath}) => {
          let variableValue: string;
          if (responsePath === 'response' && isResponseBodyText) {
            variableValue = responseBody;
          } else if (startsWith(responsePath, 'response.') && !isResponseBodyText) {
            const getter = replace(responsePath, 'response.', '');
            const responseObj = JSON.parse(responseBody);
            const varValue = get(responseObj, getter, null);

            if (varValue) {
              const existingVar = find(envVariables, { key: variable });
              if (existingVar) {
                existingVar.value = varValue;
              } else {
                envVariables.push({ key: variable, value: varValue });
              }
            }
          }
        });

        draftCollection.envVariables = envVariables;
        return updateDraftCollection({ payload: draftCollection });
      })
    )
  );
}