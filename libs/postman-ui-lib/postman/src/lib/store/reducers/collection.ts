import { Action, createReducer, on } from '@ngrx/store';
import { isEqual, reduce, cloneDeep, isEmpty } from 'lodash';

import { ApiCollection, CollectionItem } from '../../models/collection';
import { ApiRequest } from '../../models/api-request';
import * as CollectionActions from '../actions/collection';


export interface State {
  originalCollection: ApiCollection | null;
  draftCollection: ApiCollection | null;
  errorText: string;
}

const initialState: State = {
  originalCollection: null,
  draftCollection: null,
  errorText: ''
};

const collectionReducer = createReducer(
  initialState,
  on(CollectionActions.exportCollection, (state) => ({
    ...state,
    originalCollection: cloneDeep(state.draftCollection)
  })),
  on(CollectionActions.loadCollection, (state, { payload }) => ({
    ...state,
    errorText: '',
    originalCollection: { ...payload },
    draftCollection: { ...payload }
  })),
  on(CollectionActions.importFileError, (state, { errorText, reset = true }) => ({
    ...state,
    errorText,
    originalCollection: reset ? null : state.originalCollection,
    draftCollection: reset ? null : state.draftCollection
  })),
  on(CollectionActions.saveEnvVariables, (state, { envVariables }) => ({
    ...state,
    draftCollection: {
      ...state.draftCollection,
      envVariables
    }
  })),
  on(CollectionActions.updateDraftCollection, (state, { payload }) => ({
    ...state,
    draftCollection: { ...payload }
  }))
);

export function reducer(state: State, action: Action) {
  return collectionReducer(state, action);
}

export const getDraftCollection = (state: State) => {
  const { draftCollection } = state;
  return isEmpty(draftCollection) ? null : cloneDeep(draftCollection);
};

export const isCollectionDirty = (state: State) => {
  const { draftCollection, originalCollection } = state;
  return draftCollection ? !isEqual(originalCollection, draftCollection) : false;
};
export const totalRequests = (state: State) => (state.draftCollection ? countRequests(state.draftCollection.item) : 0);
export const getErrorText = (state: State) => state.errorText;

/**
 * get all requests in a collection item
 * @param item - collection item
 */
function countRequests(collectionItems: Array<CollectionItem | ApiRequest>) {
  return reduce(collectionItems, (accu, item) => accu + (('item' in item) ? countRequests(item.item) : 1), 0);
}
