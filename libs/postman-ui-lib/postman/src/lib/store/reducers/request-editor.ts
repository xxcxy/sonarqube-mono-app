import { Action, createReducer, on } from '@ngrx/store';
import { isEqual, cloneDeep, isEmpty } from 'lodash';

import { ApiRequest } from '../../models/api-request';
import * as requestActions from '../actions/request-editor';


export interface State {
  originalRequest: ApiRequest;
  draftRequest: ApiRequest;
  isSendingRequest: boolean;
}

const initialState: State = {
  originalRequest: null,
  draftRequest: null,
  isSendingRequest: false
};

const requestEditorReducer = createReducer(
  initialState,
  on(requestActions.selectRequest, (state, { request }) => ({
    ...state,
    originalRequest: request ? { ...request } : null,
    draftRequest: request ? { ...request } : null
  })),
  on(requestActions.updateRequest, (state, { request }) => ({
    ...state,
    draftRequest: { ...request }
  })),
  on(requestActions.saveRequest, (state) => ({
    ...state,
    originalRequest: cloneDeep(state.draftRequest)
  })),
  on(requestActions.sendApiRequest, (state) => ({
    ...state,
    isSendingRequest: true
  })),
  on(requestActions.saveResponse, (state, { response }) => ({
    ...state,
    originalRequest: {
      ...state.originalRequest,
      response
    },
    draftRequest: {
      ...state.draftRequest,
      response
    },
    isSendingRequest: false
  })),
  on(requestActions.cancelApiRequest, (state) => ({
    ...state,
    isSendingRequest: false
  }))
);

export function reducer(state: State, action: Action) {
  return requestEditorReducer(state, action);
}

export const getOriginalRequest = (state: State) => isEmpty(state.originalRequest) ? null : cloneDeep(state.originalRequest);
export const getDraftRequest = (state: State) => isEmpty(state.draftRequest) ? null : cloneDeep(state.draftRequest);
export const isSendingRequest = (state: State) => state.isSendingRequest;

export const isRequestDirty = (state: State) => {
  const { draftRequest, originalRequest } = state;
  return draftRequest ? !isEqual(originalRequest, draftRequest) : false;
};
