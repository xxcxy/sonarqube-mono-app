import { createAction, props } from '@ngrx/store';

import { ApiCollection, KeyValueData, CollectionItem } from '../../models/collection';
import { ApiRequest } from '../../models/api-request';


export enum CollectionActionTypes {
  LoadFromStorage = '[Collection] Init from Storage',
  LoadCollection = '[Collection] Load Collection',
  ImportCollection = '[Collection] Import Collection',
  ExportCollection = '[Collection] Export Collection',
  CreateNew = '[Collection] Create New',
  SaveEnvVariables = '[Collection] Save Env Variables',
  UpdateDraftCollection = '[Collection] Update Draft Collection',
  ImportFileError = '[Collection] Import File Error',
  SaveNode = '[Collection] Save Node',
  AddFolder = '[Collection] Add Folder',
  AddRequest = '[Collection] Add Request',
  DuplicateNode = '[Collection] Duplicate Node', // could be folder or request
  DeleteNode ='[Collection] Delete Node' // could be folder or request,
}

export const loadFromStorage = createAction(
  CollectionActionTypes.LoadFromStorage
);

export const loadCollection = createAction(
  CollectionActionTypes.LoadCollection,
  props<{ payload: ApiCollection }>()
);

export const createNew = createAction(
  CollectionActionTypes.CreateNew
);

// import the collection from the passed json file
export const importCollection = createAction(
  CollectionActionTypes.ImportCollection,
  props<{ file: File }>()
);

export const exportCollection = createAction(
  CollectionActionTypes.ExportCollection
);

export const saveEnvVariables = createAction(
  CollectionActionTypes.SaveEnvVariables,
  props<{ envVariables: KeyValueData[] }>()
);

export const updateDraftCollection = createAction(
  CollectionActionTypes.UpdateDraftCollection,
  props<{ payload: ApiCollection }>()
);

export const importFileError = createAction(
  CollectionActionTypes.ImportFileError,
  props<{ errorText: string; reset?: boolean }>()
);

export const saveNode = createAction(
  CollectionActionTypes.SaveNode,
  props<{ node: CollectionItem | ApiRequest }>()
);

export const addFolder = createAction(
  CollectionActionTypes.AddFolder,
  props<{ nodeId: string}>()
);

export const addRequest = createAction(
  CollectionActionTypes.AddRequest,
  props<{ nodeId: string}>()
);

export const duplicateNode = createAction(
  CollectionActionTypes.DuplicateNode,
  props<{ node: ApiRequest | CollectionItem }>()
);

export const deleteNode = createAction(
  CollectionActionTypes.DeleteNode,
  props<{ nodeId: string}>()
);
