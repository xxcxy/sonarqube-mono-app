import { createAction, props } from '@ngrx/store';

import { ApiRequest, Response } from '../../models/api-request';


export enum RequestEditorActionTypes {
  SelectRequestInit = '[Request] Select Request Init',
  SelectRequest = '[Request] Select Request',
  UpdateRequest = '[Request] Update Request',
  SaveRequest = '[Request] Save Request',
  SendApiRequest = '[Request] Send Api Request',
  CancelApiRequest = '[Request] Cancel Api Request',
  ExecuteRequestTests = '[Request] Execute Request Tests',
  SaveResponse = '[Response] Save Response'
}

export const selectRequestInit = createAction(
  RequestEditorActionTypes.SelectRequestInit,
  props<{ node: ApiRequest }>()
);

export const selectRequest = createAction(
  RequestEditorActionTypes.SelectRequest,
  props<{ request: ApiRequest }>()
);

export const updateRequest = createAction(
  RequestEditorActionTypes.UpdateRequest,
  props<{ request: ApiRequest }>()
);

export const saveRequest = createAction(
  RequestEditorActionTypes.SaveRequest
);

export const sendApiRequest = createAction(
  RequestEditorActionTypes.SendApiRequest
);

export const cancelApiRequest = createAction(
  RequestEditorActionTypes.CancelApiRequest
);

export const executeRequestTests = createAction(
  RequestEditorActionTypes.ExecuteRequestTests,
  props<{ responseBody: string }>()
);

export const saveResponse = createAction(
  RequestEditorActionTypes.SaveResponse,
  props<{ response: Response }>()
);
