import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromCollection from './reducers/collection';
import * as fromRequestEditor from './reducers/request-editor';


export interface PostmanState {
  collection: fromCollection.State;
  requestEditor: fromRequestEditor.State;
}

export const reducers: ActionReducerMap<PostmanState> = {
  collection: fromCollection.reducer,
  requestEditor: fromRequestEditor.reducer
};

// postman state
const postmanState = createFeatureSelector<PostmanState>('postman');

// collection
const collectionState = createSelector(postmanState, (state: PostmanState) => state.collection);
export const getCollection = createSelector(collectionState, fromCollection.getDraftCollection);
export const totalRequests = createSelector(collectionState, fromCollection.totalRequests);
export const isCollectionDirty = createSelector(collectionState, fromCollection.isCollectionDirty);
export const errorText = createSelector(collectionState, fromCollection.getErrorText);

// request editor
const requestEditorState = createSelector(postmanState, (state: PostmanState) => state.requestEditor);
export const getOriginalRequest = createSelector(requestEditorState, fromRequestEditor.getOriginalRequest);
export const getSelectedRequest = createSelector(requestEditorState, fromRequestEditor.getDraftRequest);
export const isRequestDirty = createSelector(requestEditorState, fromRequestEditor.isRequestDirty);
export const isSendingRequest = createSelector(requestEditorState, fromRequestEditor.isSendingRequest);
