import { Injectable, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { of, Observable, from } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
// import moment, { Moment } from 'moment';
const moment = require('moment');
import jwtDecode from 'jwt-decode';
import { reduce, trim, isObject, keys } from 'lodash';

import { Request, RequestMethods, Response } from './models/api-request';
import { KeyValueData } from './models/collection';
import { parseAndReplace } from './utils';
import { HttpHelperService, LoggerService, FACADE_API_PREFIX } from '@pop-soda-facade/postman-ui-lib/services';


@Injectable({
  providedIn: 'root'
})
export class PostmanService {

  constructor(
    private logger: LoggerService,
    private httpHelper: HttpHelperService,
    private location: Location,
    private cookieService: CookieService,
    @Inject(FACADE_API_PREFIX) private facadeAPIPrefix
  ) { }

  sendRequest(request: Request, envVariables: KeyValueData[]): Observable<Response> {
    const { method, url: rawUrl, params, headers, body } = request;
    const enabledEnvVariables: { key: string, value: string }[] = reduce(
      envVariables,
      (accu, { key, value, disabled }) => disabled ? accu : accu.concat({ key, value }),
      []
    );
    let newUrl = rawUrl;
    if (newUrl.startsWith('/') && newUrl !== '/api/login') {
      // add the api prefix
      newUrl = `${this.facadeAPIPrefix}${newUrl}`;
    }
    const url = parseAndReplace(newUrl, enabledEnvVariables);
    this.logger.log(`Requesting data from Public Endpoint: ${url}`);

    const reqParameters = params ? reduce(params, (accu, item) => {
      const { key, value } = item;
      if (trim(key)) {
        accu[key] = parseAndReplace(value, enabledEnvVariables);
      }
      return accu;
    }, {}) : {};
    
    const reqHeader = headers ? reduce(headers, (accu, item) => {
      const { key, value } = item;
      if (trim(key)) {
        accu[key] = parseAndReplace(value, enabledEnvVariables);
      }
      return accu;
    }, {}) : {};
    
    let reqBody: any = {};
    if (body) {
      switch (body.mode) {
        case 'json':
          try {
            const jsonStr = parseAndReplace(body.jsonString, enabledEnvVariables);
            reqBody = JSON.parse(jsonStr);
          } catch (e) {
            reqBody = {};
          }
          break;

        case 'form-data':
          reqBody = new FormData();
          body.formdata.filter(({ key }) => !!trim(key)).forEach(({ key, value }) => {
            reqBody.append(key, parseAndReplace(value, enabledEnvVariables));
          });
          break;

        case 'urlencoded':
          reqBody = new URLSearchParams();
          body.urlencoded.filter(({ key }) => !!trim(key)).forEach(({ key, value }) => {
            reqBody.set(key, parseAndReplace(value, enabledEnvVariables));
          });
          break;

        default:
          reqBody = {};
      }
    }

    const startTime = moment();
    let response: Observable<Response>;
    switch (method) {
      case RequestMethods.GET:
        response = from(this.httpHelper.get(url, { headers: reqHeader, params: reqParameters }));
        break;

      case RequestMethods.POST:
        response = from(this.httpHelper.post(url, reqBody, { headers: reqHeader, params: reqParameters }));
        break;

      default:
        response = from(this.httpHelper.request(method.toLocaleLowerCase(), url, reqBody, { headers: reqHeader, params: reqParameters }));
    }

    return response.pipe(
      map(res => this.formResponseObject(res, startTime)),
      catchError((e) => of(this.formResponseObject(e, startTime)))
    );
  }

  private formResponseObject(response: any, startTime: any) {
    const { headers, status, body, message } = response;

    const processedHeaders = keys((headers as HttpHeaders)).map(headerKey => ({
      key: headerKey,
      value: headers.get(headerKey),
      description: ''
    }));
    
    const cookies = Object.entries<string>(this.cookieService.getAll());
    const decodedCookies = cookies.map((c) => jwtDecode(c[1]));
    const processedCookies = decodedCookies.map((c, i) => ({
      name: cookies[i][0],
      value: cookies[i][1],
      domain: c.iss,
      path: this.location.path(),
      expires: c.exp
    }));

    return {
      status,
      time: moment().diff(startTime),
      size: Number(response.headers.get('content-length')),
      headers: processedHeaders,
      cookies: processedCookies,
      body: body ? (isObject(body) ? JSON.stringify(body) : body) : message
    };
  }
}
