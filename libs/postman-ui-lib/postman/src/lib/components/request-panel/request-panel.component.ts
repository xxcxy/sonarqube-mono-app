import { Component, OnChanges, Input, SimpleChanges, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { values, cloneDeep, isEqual, trim } from 'lodash';

import { ApiRequest, RequestMethods, TestsData } from '../../models/api-request';
import * as postmanStore from '../../store';
import * as requestEditorActions from '../../store/actions/request-editor';


@Component({
  selector: 'ui-request-panel',
  templateUrl: './request-panel.component.html',
  styleUrls: ['./request-panel.component.scss']
})
export class RequestPanelComponent implements OnChanges {

  @Input() apiRequest: ApiRequest;
  @Input() isRequestDirty = false;
  @Input() isSendingRequest: boolean;

  @ViewChild('reqName', { static: false }) reqNameInput: ElementRef;
  @ViewChild('reqDescription', { static: false }) reqDescInput: ElementRef;

  requestMethods: string[] = values(RequestMethods);

  apiRequestLocal: ApiRequest;
  isDescriptionExpanded = false;
  descriptionInEdit = false;
  nameInEdit = false;
  requestActiveTabIndex = 0;
  responseActiveTabIndex = 0;

  testsData: TestsData[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private store: Store<postmanStore.PostmanState>
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.apiRequest) {
      if (!this.apiRequest || (!this.apiRequestLocal || this.apiRequestLocal.id !== this.apiRequest.id)) {
        this.resetEditor();
      }

      if (this.apiRequestLocal && this.apiRequest && !isEqual(this.apiRequestLocal.response, this.apiRequest.response)) {
        this.responseActiveTabIndex = 0;
        this.apiRequestLocal.response = { ...this.apiRequest.response };
      }
    }
  }

  resetEditor() {
    this.apiRequestLocal = cloneDeep(this.apiRequest);
    this.isDescriptionExpanded = false;
    this.descriptionInEdit = false;
    this.nameInEdit = false;
    this.requestActiveTabIndex = 0;
    this.responseActiveTabIndex = 0;
  }

  setNameInEdit(e: Event) {
    this.nameInEdit = true;
    this.cdr.detectChanges();
    this.reqNameInput.nativeElement.focus();
    e.stopPropagation();
  }

  setDescriptionInEdit(e: Event) {
    this.descriptionInEdit = true;
    this.cdr.detectChanges();
    this.reqDescInput.nativeElement.focus();
    e.stopPropagation();
  }

  saveRequestName() {
    this.nameInEdit = false;

    if (trim(this.apiRequestLocal.name)) {
      this.updateApiRequest();
    } else {
      this.apiRequestLocal.name = this.apiRequest.name;
    }
  }

  saveDescription() {
    this.descriptionInEdit = false;
    this.updateApiRequest();
  }

  updateApiTests() {
    if (!this.apiRequestLocal.tests && this.testsData.length > 0) {
      this.apiRequestLocal.tests =  [ ...this.testsData ];
    }

    this.updateApiRequest();
  }

  updateApiRequest() {
    this.store.dispatch(requestEditorActions.updateRequest({ request: cloneDeep(this.apiRequestLocal) }));
  }

  saveApiRequest() {
    this.store.dispatch(requestEditorActions.saveRequest());
  }

  sendApiRequest() {
    this.store.dispatch(requestEditorActions.sendApiRequest());
  }

  cancelApiRequest() {
    this.store.dispatch(requestEditorActions.cancelApiRequest());
  }

}
