import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestPanelComponent } from './request-panel.component';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { RequestSectionComponent } from '../request-section/request-section.component';
import { ResponseSectionComponent } from '../response-section/response-section.component';
import { MatTabsModule } from '@angular/material/tabs';
import { KeyValueDataTableComponent } from '../key-value-data-table/key-value-data-table.component';
import { MatRadioModule } from '@angular/material/radio';
import { MaterialComponentsModule } from '../../material-components/material-components.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

describe('RequestPanelComponent', () => {
  let component: RequestPanelComponent;
  let fixture: ComponentFixture<RequestPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        FormsModule,
        MatDividerModule,
        MatOptionModule,
        MatSelectModule,
        MatTabsModule,
        MatRadioModule,
        MaterialComponentsModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([])
      ],
      declarations: [
        RequestPanelComponent,
        RequestSectionComponent,
        ResponseSectionComponent,
        KeyValueDataTableComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
