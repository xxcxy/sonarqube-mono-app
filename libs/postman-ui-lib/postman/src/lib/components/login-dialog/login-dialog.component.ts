import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { LoginService } from '@pop-soda-facade/postman-ui-lib/services';

@Component({
  selector: 'ui-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit {
  public username = 'test';
  public password = 'test';
  public error;

  constructor(
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    private loginService: LoginService
  ) { }

  ngOnInit() {}

  async login () {
    try {
      this.error = undefined;
      await this.loginService.login(this.username, this.password);
      this.dialogRef.close();
    } catch (e) {
      this.error = e.message || e.toString() || 'Unknown error';
    }
  }
}
