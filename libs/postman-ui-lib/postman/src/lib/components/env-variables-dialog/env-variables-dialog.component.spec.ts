import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvVariablesDialogComponent } from './env-variables-dialog.component';
import { KeyValueDataTableComponent } from '../key-value-data-table/key-value-data-table.component';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

describe('EnvVariablesDialogComponent', () => {
  let component: EnvVariablesDialogComponent;
  let fixture: ComponentFixture<EnvVariablesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        MatCheckboxModule,
        FormsModule,
        MatIconModule
      ],
      declarations: [
        EnvVariablesDialogComponent,
        KeyValueDataTableComponent
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] }
      ],

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvVariablesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
