import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { KeyValueData } from '../../models/collection';

interface DialogData {
  envVariables: KeyValueData[];
}


@Component({
  selector: 'ui-env-variables-dialog',
  templateUrl: './env-variables-dialog.component.html',
  styleUrls: ['./env-variables-dialog.component.scss']
})
export class EnvVariablesDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EnvVariablesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
  }

}
