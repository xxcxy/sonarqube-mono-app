import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { Store } from '@ngrx/store';
import { omit, includes, xor, union, difference, trim } from 'lodash';

import { EnvVariablesDialogComponent } from '../env-variables-dialog/env-variables-dialog.component';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { ApiCollection, CollectionItem } from '../../models/collection';
import { ApiRequest } from '../../models/api-request';
import * as postmanStore from '../../store';
import * as collectionActions from '../../store/actions/collection';
import { selectRequestInit } from '../../store/actions/request-editor';


@Component({
  selector: 'ui-collection-tree',
  templateUrl: './collection-tree.component.html',
  styleUrls: ['./collection-tree.component.scss']
})
export class CollectionTreeComponent implements OnChanges {

  @Input() collection: ApiCollection;
  @Input() totalRequests = 0;
  @Input() selectedRequestId: string;

  expandedNodeIds: string[] = [];
  rootNodeId: string;
  nodeInEdit: ApiRequest | CollectionItem;

  treeControl = new NestedTreeControl<CollectionItem | ApiRequest>(node => (node as CollectionItem).item);
  dataSource = new MatTreeNestedDataSource<CollectionItem | ApiRequest>();

  constructor(
    private store: Store<postmanStore.PostmanState>,
    private dialogService: MatDialog
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.collection) {
      this.dataSource.data = this.collection ? [omit(this.collection, 'envVariables')] : null;
      this.rootNodeId = this.collection ? this.collection.id : undefined;

      // reset expanded node ids when collection is changed
      // we find that out when root node's id changes
      if (!!changes.collection.previousValue && (!this.collection || changes.collection.previousValue.id !== this.collection.id)) {
        this.expandedNodeIds = [];
      } 
    }
  }

  isNodeExpanded(nodeId: string) {
    return includes(this.expandedNodeIds, nodeId);
  }

  hasChild(_: number, node: CollectionItem) {
    return !!node.item;
  }

  toggleNode(nodeId: string) {
    this.saveNodeName();
    this.expandedNodeIds = xor(this.expandedNodeIds, [nodeId]);
  }

  addFolder(nodeId: string) {
    this.store.dispatch(collectionActions.addFolder({ nodeId }));
    this.expandedNodeIds = union(this.expandedNodeIds, [nodeId]);
  }

  addRequest(nodeId: string) {
    this.store.dispatch(collectionActions.addRequest({ nodeId }));
    this.expandedNodeIds = union(this.expandedNodeIds, [nodeId]);
  }

  selectRequest(node: ApiRequest) {
    this.saveNodeName();
    this.store.dispatch(selectRequestInit({ node }));
  }

  duplicateNode(node: ApiRequest | CollectionItem) {
    this.store.dispatch(collectionActions.duplicateNode({ node }));
  }

  renameNode(node: ApiRequest | CollectionItem, e: Event) {
    this.nodeInEdit = { ...node };
    e.stopPropagation();
  }

  saveNodeName() {
    if (!this.nodeInEdit) {
      return;
    }

    if (trim(this.nodeInEdit.name)) {
      this.store.dispatch(collectionActions.saveNode({ node: this.nodeInEdit }));
    }

    this.nodeInEdit = null;
  }

  deleteNode(node: ApiRequest | CollectionItem) {
    const isLeaf = !('item' in node);

    const dialogRef = this.dialogService.open(ConfirmationDialogComponent, {
      width: '350px',
      data: {
        headerText: `Delete ${isLeaf ? 'Request' : 'Folder'}`,
        confirmText: `Are you sure you want to delete this ${isLeaf ? 'request' : 'folder'} ?`
      }
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response === 'confirm') {
        if (!isLeaf) {
          this.expandedNodeIds = difference(this.expandedNodeIds, [node.id]);
        }

        this.store.dispatch(collectionActions.deleteNode({ nodeId: node.id }));
      }
    });
  }

  updateEnvironmentVariables() {
    const dialogRef = this.dialogService.open(EnvVariablesDialogComponent, {
      width: '70vw',
      data: {
        envVariables: [ ...this.collection.envVariables ]
      }
    });

    dialogRef.afterClosed().subscribe(envVariables => {
      if (envVariables) {
        this.store.dispatch(collectionActions.saveEnvVariables({ envVariables }));
      }
    });
  }

}
