import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestSectionComponent } from './request-section.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { KeyValueDataTableComponent } from '../key-value-data-table/key-value-data-table.component';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { RequestMethods } from '../../models/api-request';

describe('RequestSectionComponent', () => {
  let component: RequestSectionComponent;
  let fixture: ComponentFixture<RequestSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatTabsModule,
        MatIconModule,
        MatRadioModule,
        FormsModule,
        MatButtonModule,
        MatCheckboxModule,
        NoopAnimationsModule
      ],
      declarations: [ RequestSectionComponent, KeyValueDataTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestSectionComponent);
    component = fixture.componentInstance;
    component.request = {
      method: RequestMethods.GET,
      url: ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
