import { Component, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

import { Request, RequestBodyModes, TestsData } from '../../models/api-request';
import { KeyValueData } from '../../models/collection';
import { isJsonValid } from '../../utils';

@Component({
  selector: 'ui-request-section',
  templateUrl: './request-section.component.html',
  styleUrls: ['./request-section.component.scss']
})
export class RequestSectionComponent implements OnChanges {

  @Input() request: Request;
  @Input() requestTests: TestsData[];
  @Input() activeTabIndex = 0;

  @Output() activeTabIndexChange = new EventEmitter<number>();
  @Output() requestUpdated = new EventEmitter<void>();
  @Output() testsUpdated = new EventEmitter<void>();

  requestParams: KeyValueData[] = [];
  requestHeaders: KeyValueData[] = [];
  requestBody: { jsonString: string, formdata: KeyValueData[], urlencoded: KeyValueData[] };

  testsCols = [
    { key: 'variable', text: 'Environment Variable' },
    { key: 'responsePath', text: 'Response Path' }
  ];
  
  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.request) {
      this.requestBody = {
        jsonString: this.request.body && 'jsonString' in this.request.body ? this.request.body.jsonString : '',
        formdata: [],
        urlencoded: []
      };
    }
  }

  changeTab(index: number) {
    this.activeTabIndexChange.emit(index);
  }

  checkJsonValidity(str: string) {
    return isJsonValid(str);
  }

  requestBodyChanged(mode: 'none' | RequestBodyModes) {
    switch (mode) {
      case RequestBodyModes.JSON:
        this.request.body = {
          mode,
          jsonString: this.request.body && 'jsonString' in this.request.body ? this.request.body.jsonString : this.requestBody.jsonString
        };
        break;

      case RequestBodyModes.FORMDATA:
        this.request.body = {
          mode,
          formdata: this.request.body && 'formdata' in this.request.body ? this.request.body.formdata : this.requestBody.formdata
        };
        break;

      case RequestBodyModes.URLENCODED:
        this.request.body = {
          mode,
          urlencoded: this.request.body && 'urlencoded' in this.request.body ? this.request.body.urlencoded : this.requestBody.urlencoded
        };
        break;

      default:
        delete this.request.body;
    }

    this.onRequestUpdated();
  }

  updateRequestBodyJson(value: string) {
    if ('jsonString' in this.request.body) {
      this.request.body.jsonString = value;
    }
    this.requestBody.jsonString = value;

    this.requestBodyChanged(RequestBodyModes.JSON);
  }

  onRequestUpdated() {
    if (!this.request.headers && this.requestHeaders.length > 0) {
      this.request.headers = this.requestHeaders;
    }
    if (!this.request.params && this.requestParams.length > 0) {
      this.request.params = this.requestParams;
    }
    this.requestUpdated.emit();
  }

  onRequestTestsUpdated() {
    this.testsUpdated.emit();
  }

}
