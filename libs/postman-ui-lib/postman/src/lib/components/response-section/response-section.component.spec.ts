import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponseSectionComponent } from './response-section.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatIconModule } from '@angular/material/icon';
import { KeyValueDataTableComponent } from '../key-value-data-table/key-value-data-table.component';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('ResponseSectionComponent', () => {
  let component: ResponseSectionComponent;
  let fixture: ComponentFixture<ResponseSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatDividerModule,
        MatTabsModule,
        MatButtonToggleModule,
        ClipboardModule,
        MatIconModule,
        FormsModule,
        MatCheckboxModule,
        MatSnackBarModule
      ],
      declarations: [
        ResponseSectionComponent,
        KeyValueDataTableComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponseSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
