import { Component, OnChanges, Input, Output, EventEmitter, SimpleChanges, SimpleChange } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Response } from '../../models/api-request';


@Component({
  selector: 'ui-response-section',
  templateUrl: './response-section.component.html',
  styleUrls: ['./response-section.component.scss']
})
export class ResponseSectionComponent implements OnChanges {

  @Input() response: Response;
  @Input() activeTabIndex = 0;
  @Output() activeTabIndexChange = new EventEmitter<number>();

  bodyText: string;
  bodyType: 'json' | 'raw';

  cookieTableCols = ['name', 'value', 'domain', 'path'];
  
  constructor(
    private snackBar: MatSnackBar
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.response && this.response) {
      this.changeBodyType('json');
    }
  }

  changeTab(index: number) {
    this.activeTabIndexChange.emit(index);
  }

  copyToClipSuccess() {
    this.snackBar.open('Copied to clipboard!', 'Dismiss', { duration: 3000 });
  }

  changeBodyType(type: 'json' | 'raw') {
    this.bodyType = type;

    if (type === 'json') {
      try {
        this.bodyText = JSON.stringify(JSON.parse(this.response.body), null, 4);
      } catch (e) {
        this.bodyText = this.response.body;
      }
    } else {
      this.bodyText = this.response.body;
    }
  }

}
