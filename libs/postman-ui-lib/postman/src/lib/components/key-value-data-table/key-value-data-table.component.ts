import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { reduce } from 'lodash';



@Component({
  selector: 'ui-key-value-data-table',
  templateUrl: './key-value-data-table.component.html',
  styleUrls: ['./key-value-data-table.component.scss']
})
export class KeyValueDataTableComponent implements OnInit {

  @Input() data: any[];
  @Input() readonly = false;
  // default columns are defined
  @Input() cols?: Array<{ key: string; text: string; }> = [
    { key: 'key', text: 'Key' },
    { key: 'value', text: 'Value' },
    { key: 'description', text: 'Description '}
  ];
  @Output() dataUpdated = new EventEmitter<void>();
  
  focusedRowId: number;

  constructor() { }

  ngOnInit() {
  }

  addRow() {
    const newObj = reduce(this.cols, (accu, { key }) => {
      accu[key] = '';
      return accu;
    }, {});
    this.data.push(newObj);
    this.onTableUpdated();
  }

  deleteRow(index: number) {
    this.data.splice(index, 1);
    this.onTableUpdated();
  }

  onTableUpdated() {
    this.dataUpdated.emit();
  }

}
