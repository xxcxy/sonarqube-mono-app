import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyValueDataTableComponent } from './key-value-data-table.component';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';

describe('KeyValueDataTableComponent', () => {
  let component: KeyValueDataTableComponent;
  let fixture: ComponentFixture<KeyValueDataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatIconModule, FormsModule, MatCheckboxModule],
      declarations: [ KeyValueDataTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyValueDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
