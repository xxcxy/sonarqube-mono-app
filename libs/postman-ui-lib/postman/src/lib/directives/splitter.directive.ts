import { Directive, Input, OnDestroy, OnInit, ElementRef, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import Split from 'split.js';


/*
  To allow for resizing of sections
  The gutter is always 1px wide or long
  and it is enchanced by adding some absolute positioned width or height
 */
@Directive({
  selector: '[uiSplit]'
})
export class SplitDirective implements OnDestroy, OnChanges {

  @Input() private splitDirection = 'horizontal';
  @Input() private splitFlexLayout = false;
  @Input() private splitEnabled ? = true;

  private areas = [];
  private build$ = new Subject<boolean>();
  private sub: Subscription;
  private splitInstance: any;

  /**
   * Constructor.
   */
  constructor() {
    this.sub = this.build$.subscribe(() => this.splitEnabled && this.split());
  }

  /*
    when splitEnabled changes, either enable split directive or destroy it
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes['splitEnabled']) {
      if (this.splitEnabled) {
        this.build$.next(true);
      } else {
        if (this.splitInstance) {
          this.splitInstance.destroy();
          this.splitInstance = undefined;
        }
      }
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  /**
   * Add area.
   * @param area the split area to add
   */
  public addArea(area) {
    this.areas.push(area);
    this.build$.next(true);
  }

  /**
   * Split areas.
   */
  private split() {
    if (this.areas.length < 2) {
      // At least 2 areas needed to split
      return;
    }

    const options = this.getOptions();
    options.sizes = this.areas.map(area => area.size);
    options.minSize = this.areas.map(area => area.minSize || 0);

    this.splitInstance = Split(this.areas.map(area => area.element), options);
  }

  /**
   * Get options.
   * @returns options
   */
  private getOptions() {

    const options: any = { direction: this.splitDirection, snapOffset: 0, gutterSize: 1, expandToMin: true };

    if (this.splitFlexLayout) {
      options.elementStyle = function (_dimension, size, gutterSize) {
        return {
          'flex-basis': 'calc(' + size + '% - ' + gutterSize + 'px)',
        };
      };
    }

    return options;
  }
}

@Directive({
  selector: '[uiSplitArea]'
})
export class SplitAreaDirective implements OnInit {
  @Input() splitSize: number;
  @Input() minSize: number;

  /**
   * Constructor.
   * @param split The parent split directive
   * @param el The element reference to this area
   */
  constructor(private split: SplitDirective, private el: ElementRef) { }

  /**
   * Initialize the component.
   */
  public ngOnInit() {
    const element = this.el.nativeElement;
    const getBoundingClientRect$ = element.getBoundingClientRect;

    let first = true;
    element.getBoundingClientRect = () => {
      if (first) {
        // First call to getBoundingClientRect is used to align min size
        // , we'll ensure our min size is used
        first = false;
        return {
          height: this.minSize,
          width: this.minSize,
        };
      }
      return getBoundingClientRect$.apply(element);
    };

    // Add this area to split
    this.split.addArea({
      size: this.splitSize,
      minSize: this.minSize,
      element,
    });
  }
}
