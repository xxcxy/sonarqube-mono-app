import { TestBed } from '@angular/core/testing';

import { PostmanService } from './postman.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { FACADE_API_PREFIX } from '@pop-soda-facade/postman-ui-lib/services';

describe('PostmanService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        CookieService,
        { provide: FACADE_API_PREFIX, useValue: '' }
      ]
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    const service: PostmanService = TestBed.inject(PostmanService);
    expect(service).toBeTruthy();
  });
});
