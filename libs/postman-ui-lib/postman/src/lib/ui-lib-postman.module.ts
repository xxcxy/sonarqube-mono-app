import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FlexLayoutModule } from '@angular/flex-layout';

// ngrx store, reducers, effects
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducers } from './store';
import AllEffects from './store/effects';

// material components module
import { MaterialComponentsModule } from './material-components/material-components.module';

// directives
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { SplitDirective, SplitAreaDirective } from './directives/splitter.directive';

// bootstrap component
import { PostmanComponent } from './postman.component';

import { RequestPanelComponent } from './components/request-panel/request-panel.component';
import { EnvVariablesDialogComponent } from './components/env-variables-dialog/env-variables-dialog.component';
import { CollectionTreeComponent } from './components/collection-tree/collection-tree.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { ResponseSectionComponent } from './components/response-section/response-section.component';
import { RequestSectionComponent } from './components/request-section/request-section.component';
import { KeyValueDataTableComponent } from './components/key-value-data-table/key-value-data-table.component';
import { LoginDialogComponent } from './components/login-dialog/login-dialog.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: PostmanComponent }
    ]),
    FlexLayoutModule,
    MaterialComponentsModule,
    StoreModule.forFeature('postman', reducers),
    EffectsModule.forFeature(AllEffects)
  ],
  declarations: [
    PostmanComponent,
    SplitDirective,
    SplitAreaDirective,
    RequestPanelComponent,
    EnvVariablesDialogComponent,
    LoginDialogComponent,
    CollectionTreeComponent,
    ConfirmationDialogComponent,
    ResponseSectionComponent,
    RequestSectionComponent,
    KeyValueDataTableComponent,
    ClickOutsideDirective
  ]
})
export class UiLibPostmanModule {}
