import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { CollectionTreeComponent } from './components/collection-tree/collection-tree.component';
import { SplitDirective, SplitAreaDirective } from './directives/splitter.directive';
import { RequestPanelComponent } from './components/request-panel/request-panel.component';
import { MatTreeModule } from '@angular/material/tree';
import { MatMenuModule } from '@angular/material/menu';
import { FormsModule } from '@angular/forms';
import { ResponseSectionComponent } from './components/response-section/response-section.component';
import { RequestSectionComponent } from './components/request-section/request-section.component';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { KeyValueDataTableComponent } from './components/key-value-data-table/key-value-data-table.component';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';


import { PostmanComponent } from './postman.component';
describe('PostmanComponent', () => {
  let component: PostmanComponent;
  let fixture: ComponentFixture<PostmanComponent>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatToolbarModule,
        FlexLayoutModule,
        MatIconModule,
        MatDividerModule,
        MatTreeModule,
        MatMenuModule,
        FormsModule,
        MatSelectModule,
        MatTabsModule,
        MatButtonToggleModule,
        MatButtonModule,
        ClipboardModule,
        MatRadioModule,
        MatCheckboxModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
        MatDialogModule,
        HttpClientTestingModule,
        RouterModule.forRoot([]),
      ],
      declarations: [
        PostmanComponent,
        CollectionTreeComponent,
        SplitDirective,
        SplitAreaDirective,
        RequestPanelComponent,
        ResponseSectionComponent,
        RequestSectionComponent,
        KeyValueDataTableComponent,
      ],
      providers: [
        CookieService,
        { provide: APP_BASE_HREF, useValue: '/my/app' }
      ]
    })
    .compileComponents();
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostmanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
