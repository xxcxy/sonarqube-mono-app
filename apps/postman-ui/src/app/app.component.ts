import { Component, OnInit, HostBinding, Inject } from '@angular/core';

import { StartupService } from '@pop-soda-facade/postman-ui-lib/services';
import { DOCUMENT } from '@angular/common';
import { OverlayContainer } from '@angular/cdk/overlay';

const THEME_DARKNESS_SUFFIX = `-dark`;

@Component({
  selector: 'ui-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  @HostBinding('class') activeThemeCssClass: string;
  isThemeDark = false;
  activeTheme: string;

  initApp = false;

  constructor(
    private startup: StartupService,
    @Inject(DOCUMENT) private document: Document,
    private overlayContainer: OverlayContainer
  ) {
    this.startup.startupData.subscribe(() => {
      this.initApp = true;
    });
    const isUAT = (this.document.location.host === 'uat.local.com');
    const theme = isUAT ? 'purple-green' : 'deeppurple-amber';
    this.setActiveTheme(theme, false);
  }

  setActiveTheme(theme: string, darkness: boolean = null) {
    if (darkness === null) {
      darkness = this.isThemeDark;
    } else if (this.isThemeDark === darkness) {
      if (this.activeTheme === theme) return;
    } else {
      this.isThemeDark = darkness;
    }
    this.activeTheme = theme;

    const cssClass = darkness === true ? theme + THEME_DARKNESS_SUFFIX : theme;

    const classList = this.overlayContainer.getContainerElement().classList;
    if (classList.contains(this.activeThemeCssClass))
      classList.replace(this.activeThemeCssClass, cssClass);
    else
      classList.add(cssClass);

    this.activeThemeCssClass = cssClass;
  }


  ngOnInit() {}

}
