import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF, PlatformLocation } from "@angular/common";
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { CookieService } from 'ngx-cookie-service';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { EffectsModule } from "@ngrx/effects";


import { StartupService, FACADE_API_PREFIX } from '@pop-soda-facade/postman-ui-lib/services';
import { ROUTES } from '@pop-soda-facade/postman-ui-lib/routes';

import { AppComponent } from './app.component';

import { environment } from "../environments/environment";

export function startupServiceFactory(startupService: StartupService): Function {
  return () => startupService.load();
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES),
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [
    StartupService,
    CookieService,
    {
      provide: APP_BASE_HREF,
      useFactory: (s: PlatformLocation) => s.getBaseHrefFromDOM(),
      deps: [PlatformLocation]
    },
    {
      provide: APP_INITIALIZER,
      useFactory: startupServiceFactory,
      deps: [StartupService],
      multi: true
    },
    { provide: FACADE_API_PREFIX, useValue: environment.facadeAPIPrefix }
  ],
  bootstrap: [AppComponent],
  declarations: [AppComponent]
})
export class AppModule {}
