import * as path from 'path';
import * as _ from 'lodash';
import {describe, it} from 'mocha';
import * as chai from 'chai';
import {expect} from 'chai';

import appConfig from '@pop-soda-facade/app-config';
import {SwaggerAPI} from '@pop-soda-facade/facade-swagger-test-lib';
import * as APIs from '@pop-soda-facade/shared/api';
import {getSwaggerFilePath} from '@pop-soda-facade/shared/api';

chai.use(require('chai-json-schema'));

interface IRequest {
    method: string;
    url: string;
    params: {
        location: string
    };
}

describe('Facade Location API verification with YAML swagger', () => {

    const commonConfig = {
        baseURL: 'https://ui.local.com:4210/api/facade',
        authURL: 'https://ui.local.com:4210/api/auth'
    };

    const commonProtectedConfig = {
        ...commonConfig,
        loginURL: 'https://ui.local.com:4210/api/login',
        loginUsername: appConfig.USERS[0].USERNAME,
        loginPassword: appConfig.USERS[0].PASSWORD,
        protected: true
    };

    const defaultRequest:IRequest = {method:'GET',url:'',params:{location:'London'}};

    describe('Test Sample Facade APIs with YAML swagger format', () => {

        const swaggerAPI = new SwaggerAPI({
            ...commonConfig,
            swaggerFile: getSwaggerFilePath('api.locationsearch.yaml')
        });

        it('should return 200 with formatted body', async () => {

            const request:IRequest = _.merge(defaultRequest,{url: '/weather/locationsearch'});

            const expectedStatusCode = 200;
            const expectedResponse = await swaggerAPI.getSchemaFor(request.method, request.url, expectedStatusCode);

            const response = await swaggerAPI.send(request);

            expect(response.status).to.equal(200);
            expect(response.data).to.be.a['jsonSchema'](expectedResponse);
        });

    });

    describe('Lat-long API with Location Search (Unauthenticated)', () => {

        const swaggerAPI = new SwaggerAPI({
            ...commonConfig,
            swaggerFile: getSwaggerFilePath('api.locationsearch-latlong.yaml')
        });

        it('should return 200 with formatted body', async () => {

            const request:IRequest = _.merge(defaultRequest,{url:'/weather/locationsearch/latlong'});

            const expectedStatusCode = 200;
            const expectedResponse = await swaggerAPI.getSchemaFor(request.method, request.url, expectedStatusCode);

            const response = await swaggerAPI.send(request);

            expect(response.status).to.equal(200);
            expect(response.data).to.be.a['jsonSchema'](expectedResponse);
        });
    });

    describe('Lat-long API with Location Search (Authenticated)', () => {

        const swaggerAPI = new SwaggerAPI({
            ...commonProtectedConfig,
            swaggerFile: getSwaggerFilePath('api.locationsearch-latlong.protected.json')
        });
        

        it('should return 200 with formatted body', async () => {

            const request:IRequest = _.merge(defaultRequest,{url:'/protected/weather/locationsearch/latlong'});

            const expectedStatusCode:Number = 200;
            const expectedResponse = await swaggerAPI.getSchemaFor(request.method, request.url, expectedStatusCode);

            const response = await swaggerAPI.send(request);

            expect(response.status).to.equal(200);
            expect(response.data).to.be.a['jsonSchema'](expectedResponse);
        });
    });

});
