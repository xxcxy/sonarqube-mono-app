module.exports = {
  name: "soda-pop-ui",
  preset: "../../jest.config.js",
  coverageDirectory: "../../coverage/apps/soda-pop-ui",
  snapshotSerializers: [
    "jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js",
    "jest-preset-angular/build/AngularSnapshotSerializer.js",
    "jest-preset-angular/build/HTMLCommentSerializer.js"
  ]
};
