import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent, UiLibPagesModule } from '@pop-soda-facade/soda-pop-ui-lib/pages';
import { StartupService, FACADE_API_PREFIX } from '@pop-soda-facade/postman-ui-lib/services';
import { ROUTES } from '@pop-soda-facade/soda-pop-ui-lib/routes';
import { environment } from '../environments/environment';

export function startupServiceFactory(startupService: StartupService): Function {
  return () => startupService.load();
}
@NgModule({
  imports: [
    BrowserModule,
    UiLibPagesModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    StartupService,
    {
      provide: APP_INITIALIZER,
      useFactory: startupServiceFactory,
      deps: [StartupService],
      multi: true
    },
    { provide: FACADE_API_PREFIX, useValue: environment.facadeAPIPrefix }
  ],
  bootstrap: [DashboardComponent]
})
export class AppModule { }
