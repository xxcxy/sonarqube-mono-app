import dotenv from "dotenv";

dotenv.config();

import Hapi from "@hapi/hapi";
import * as H2O2 from "@hapi/h2o2";

import config from "@pop-soda-facade/app-config";
import path from "path";
import Inert from "@hapi/inert";
import { logger, logRequest } from "@pop-soda-facade/utils";

import { StaticFilePlugin } from "@pop-soda-facade/facade-service/static-file-plugin";
import { APIPlugin } from "@pop-soda-facade/facade-service/api-plugin";
import each from "lodash/each";
import { PassThrough } from "stream";
import { merge } from "lodash";

/**
 * Start Hapi server.
 * @returns Hapi server
 */
export async function start() {
  // Create Hapi server
  const server = new Hapi.Server({
    port: config.FACADE_SERVICE_PORT,
    tls: {
      key: config.API_KEY,
      cert: config.API_CERTIFICATE
    },
    routes: {
      payload: {
        allow: ["application/json"],
        maxBytes: config.MAX_PAYLOAD_SIZE || 1024 * 1024,
        multipart: false
      },
      files: {
        relativeTo: path.join(__dirname)
      },
      cors: {
        origin: ["localhost"]
      }
    }
  });

  // Register plugins
  await server.register([H2O2, Inert, StaticFilePlugin, APIPlugin]);

  server.ext("onPreResponse", async (request, h) => {
    const response = request.response;
    if (response["variety"] === "stream") {
      await new Promise((resolve, reject) => {
        const passThrough = response["source"].pipe(new PassThrough());
        const passThrough2 = response["source"].pipe(new PassThrough());
        const chunks = [];
        passThrough.on("data", chunk => chunks.push(chunk));
        passThrough.on("end", () => {
          request.app["payload"] = chunks.toString();
          merge(response["source"], passThrough2);
          resolve(chunks.toString());
        });
        passThrough.on("error", reject);
      });
    }
    if (
      !response["isBoom"] ||
      response["output"].statusCode !== 404
      ) {
        return h.continue;
      }
    const indexPath = path.resolve(__dirname, config.DIST_PATH, "index.html");
    return h["file"](indexPath, { confine: false });
  });

  server.events.on("response", request => {
    logRequest(request);
  });

  each(config.COOKIES, (props, cookie) => {
    server.state(cookie, {
      ...props
    });
  });

  // Start server
  await server.start();
  logger.info(`Hapi server running at: ${config.FACADE_SERVICE_PORT}`);

  return server;
}

start();
