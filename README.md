## Create a sonarqube project for each app in NX mono-repo

## Prerequisites

- Node.js 12 ( Node.js 10 would also work, but 12 is used when develop & test)

### Demo

Create a sonar project for each app, get the project-key and token and save into `sonar.json` file for each app. 
```
"<app-name>": {
    "host": "<host-url>",
    "token": "<project-token>",
    "projectName": "<project-key>"
}
```

Then you can run `nx dep-graph` to get a visualized dependency graph. Make changes in specific lib and then push the changes, you will see child jobs are created for each affected app and these child jobs are run in parallel. 

![](docs/dep-graph1.jpg)
![](docs/dep-graph2.jpg)


## Solution explanation

Please check the `.gitlab-ci.yml` file. 

1. It runs the `npm run affected:dep-graph -- --base=remotes/origin/master~1 --file .dependencies.json` to generated the affected projects. 

The `affectedProjects` node in the generated `.dependencies.json` contains the affected projects (both apps and libs). 

2. Then it runs the `node generate-job.js` script to generate the child pipeline file: `.child_job.yml`, which includes the jobs for each affected app. 

If there is no affected app, the generated pipeline file is like this:
```
empty-job:
   script:
     - echo no effected app
```

And if there are affected apps, it's like this:

```
image: 'node:12'
 cache:
   key: '${CI_COMMIT_REF_SLUG}'
   paths:
     - node_modules/
     - coverage/
 job-facade-swagger-test:
   script:
     - npm run dep-graph -- --file .dependencies.json
     - 'npm run ng run facade-swagger-test:sonar'
```

3. The child job will run app's `sonar` command defined in the `angular.json` file. 

The command will generate the sonarqube properties dynamically to include the source files of the affected app as well as the dependent libs, and then all the scanner to do the job. 

```
"sonar": {
  "builder": "@nrwl/workspace:run-commands",
  "options": {
    "commands": [
      {
        "command": "node sonar.js facade-swagger-test"
      }
    ]
  }
}


