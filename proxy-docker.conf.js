const fs = require('fs');

module.exports = {
  "/api": {
    "target": {
      "host": "web_server",
      "port": "4210",
      "protocol": "https:",
    },
    "secure": false
  },
  "/api/facade": {
    "target": {
      "host": "localhost",
      "port": "4210",
      "protocol": "https:",
    },
    "secure": false
  },
  "logLevel": "debug"
}