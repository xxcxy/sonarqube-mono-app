const sonarqubeScanner = require('sonarqube-scanner');
const dependenciesConfig = require('./.dependencies.json');
const properties = require('./sonar.json');

const analysisApp = process.argv[2];

if (!analysisApp) {
    console.log('usage: node sonar.js <project>');
    process.exit();
}
if (!properties[analysisApp]) {
    console.log(`project ${analysisApp} not exist`);
    process.exit();
}

const dependencies = dependenciesConfig.graph.dependencies;
const nodes = dependenciesConfig.graph.nodes;
const sourceSet = new Set();

function addDependencies(app, coll) {
    if (dependencies[app]) {
        dependencies[app].forEach(element => {
            addDependencies(element.target, coll);
        });
    }
    coll.add(nodes[app].data.root);
}

// collect dependencies
addDependencies(analysisApp, sourceSet);
const sourceArr = [...sourceSet];

const sourcesPath = sourceArr.map(v => `${v}/src`).join(',');
const lcovsPath = sourceArr.map(v => `coverage/${v}/lcov.info`).join(',');
const options = {
    'sonar.projectKey': properties[analysisApp].projectName,
    'sonar.projectName': properties[analysisApp].projectName,
    'sonar.sourceEncoding': 'UTF-8',
    'sonar.sources': sourcesPath,
    'sonar.exclusions': '**/node_modules/**',
    'sonar.tests': sourcesPath,
    'sonar.test.inclusions': '**/*.spec.ts*',
    'sonar.javascript.lcov.reportPaths': lcovsPath
};

if (properties[analysisApp].organization) {
    options['sonar.organization'] = properties[analysisApp].organization;
    options['sonar.projectKey'] = `${properties[analysisApp].organization}_${properties[analysisApp].projectName}`;
}

sonarqubeScanner(
  {
    serverUrl : properties[analysisApp].host,
    token : properties[analysisApp].token,
    options
  },
  () => process.exit()
)