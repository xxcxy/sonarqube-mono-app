# Open API [Open API Generator]


`package.json` includes The latest stable release of openapi-generator-cli (v 4.3.0 as of 4/15/20)<br\>
There is an expected version 5.0 to be release sometime in May 2020.

to check the version of the jar file please run `openapi-generator-cli.jar version`

This lib is used to convert an openapi specification (swagger) to a collection of typescript files<br/>  
that define interfaces and model class files to be used by both a client and server application that supports the openAPI endpoint.

To see how to use this tool try:

`openapi-generator-cli help`


`openapi-generator generate -i ./libs/shared/api/src/lib/facade/api.locationsearch.yaml -g typescript-node -o ./libs/shared/api/src/lib/generated/api.locationSearch`


[open api generator]: #open-api-generator

## [Biblio]

[openapi-generator](https://github.com/OpenAPITools/openapi-generator)
[openapi-generator-cli](https://github.com/OpenAPITools/openapi-generator-cli)
[Intellij IDE Plugin](https://plugins.jetbrains.com/plugin/8433-openapi-generator)

[Blog on openApu Generator](https://angular.schule/blog/2018-04-swagger-codegen)

