for file in ./libs/shared/api/src/lib/facade/*.{yaml,json}; do
  if [[ ! -f $file ]]; then
      continue;
  fi
  fileBaseName="${file##*/}"
  fileBaseName="${fileBaseName%.*}"
  java -jar ./tools/openapi/openapi-generator-cli.jar generate -i "$file" -g typescript-angular -o "libs/shared/api/src/lib/generated/${fileBaseName}"
done