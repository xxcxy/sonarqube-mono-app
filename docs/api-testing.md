## Testing the Demo App

1.  Use the incognito mode of your browser to test the apps.
2.  To reset the state, close all incognito windows, and open a new one.
3.  Load the UI in browser, and then import the `docs/demo-apis.json` to test the demo apis. 

#### Basic-Usage Demo APIs

The basic-usage folder contain endpoints to the Hapi Facade service that illustrate the following scenarios:

 - `Simple Passthrough Example`: This endpoint will simply access the metaweather API for location data
 and return the result without any processing.
 - `Passthrough with Lambda Example`: This endpoint will do the same as previous example, but use a simple
 lambda function to extract latitude and longitude to separate properties.  
 The lambda function can be found in `/apps/lambda-services/handler.ts`
 - `Multiple endpoint request with merge`: This endpoint will query both metaweather API and teleport API
 and provide the results of both queries.  The teleport API shows the full name of the location.
 - `Sequential Processor Example`: This endpoint will query the metaweather API for the given location, then
 sequentially use the query results to get the weather report for given location.  The weather report is
 returned.
 - `Parallel Processor Example`: This endpoint will query the metaweather API for the weather report (same as
 previous example).  It will also query teleport API for the population in that location in parallel and 
 the aggregated results are returned.

#### Spotify demo APIs

The Spotify folder contain endpoints to the Hapi Facade that allow for some simple management of the Spotify account. 
- `Spotify Login`: Use this endpoint to acquire an access token to access all other endpoints (see previous
steps for more details)
- `Get Spotify Playlist`: List all playlists available in the user account.
- `Add Spotify Playlist`: Add a new playlist to the user account.
- `Add Track to Spotify Playlist`: Add a track with given name to the playlist. The playlistId in the path should be replaced with an actual value from the get playlists api. 
- `Remove Track from Spotify Playlist`: Removes a track with given name from the playlist. The playlistId in the path should be replaced with an actual value from the get playlists api. 

The `authtoken` used in the demo apis is from a pre-created Spotify App. Refer to the [Create your own Spotify App](#create-your-own-spotify-app) section.

You need to authorize the Hapi Facade service to access your user account first. To do this, visit the following URL in your browser:
```
https://accounts.spotify.com/authorize?client_id=70d017ee69394e10ae2744e3c38b7419&response_type=code&scope=playlist-read-private playlist-modify-private&redirect_uri=http://localhost/redirect&show_dialog=true
```
  
You will be asked to authorize the Hapi Facade Service to access your user account:
![Spotify Authorize Dialog](./images/spotify-authorize.png)

Once you agree, you will be redirected to a url like: `http://localhost/redirect?code=xxxxxx`. Copy the `xxxxxx` portion and store it for use later.  

After that all you need to do is open the postman ui and add the environment variable `spotifyAuthCode`, the value being the code that you received in the previous step.
Then call the `Spotify Login` endpoint and tests in this request will auto populate the environment variables with the `access_token`, which will then be used in all the subsequent requests.
Similar, tests are already included in the demo apis when `Get Spoltify Playlist` is called which set the `playlistId` environment variable from the response.
For more information on how to set and use environement variables, and tests to auto set environment variables, please refer to the [Postman UI](#postman-ui) section.

#### Postman UI

The postman-like ui can be used to trigger requests to the `facade-service`.  

A collection can be created or imported in the application and then new requests could be added or existing requests modified. The requests could be grouped as folders.

For the expected structure of the imported collection, please refer to the models defined in the
[postman model files](../libs/postman-ui-lib/postman/src/lib/models).

Collection wide environment variables can be defined by clicking the `Environment Variables` option in the root node menu options.  
To use these environment variables, the name of the variable should be wrapped within double curly braces `{{}}`. For ex 
```
/spotify/playlists/{{playlistId}}/tracks
```

`playlistId` is the environment variable used in the request url.  

Similarly, environment variables can be used in request headers, params and body.

In the `Tests` tab in the request editor on the right side, tests could be defined in a table format to auto populate environment variables from the response received after a request is sent.  
The first column of that table should have the name of the environment variable one intends to create and the second column should have the path in the response which should be used to populate the value of the variable.  
It should always start with `response` keyword and it should be just that if the response is expected to be text and if the response is an object then dot (`.`) based path should be appended to `response`. For ex, if the response is as follows:
```
{
    "access_token": "BQA22orUgdHyR6uB-VtjJwtAYclpcUKP9knToVVEdEWDrvzD3_M4y90yevhjsibFjMlOmI0sRiDVWbbRk7x_1JkUrJ3QsZgo-_UJRRdpxO6Yf4iAKNAAev4jRnBPAqDjOAAv3790IuOSclfx_oLSJANmk5okoNhNz8dM8GvEVdzRVV5kiKE6HaCwxCiQjbai528xqR3e1vuV",
    "token_type": "Bearer",
    "expires_in": 3600,
    "refresh_token": "AQAlyfu8n_vrLvSGexcx8Fsm3ClfeVt9BRhYzvHaOnHcqS8e_IwYs6H_KTEjKTJvWbqw7WKAvZ6uzrBGa2kypAq186oih90Ww-8eH8bkM0wFrYu_ewNEz6xt10NFH8fQXwY",
    "scope": "playlist-read-private playlist-modify-private"
}
```
and if an environment variable `accessToken` is to be defined and the access_token value from the response is the intended vale then, it should look like this
```
   ENVIRONMENT VARIABLE                      RESPONSE PATH
   accessToken                               response.access_token
```

similarly if the response is as follows
```
{
    "href": "https://api.spotify.com/v1/users/ji5xoch73wryvdsn6rnqvfx87/playlists?offset=0&limit=20",
    "items": [
        {
            "id": "4Kl2OhbxKQSzTt5Yk1uqgp",
            "name": "Hapi Facade Playlist",
            "description": "API Created playlist",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/4Kl2OhbxKQSzTt5Yk1uqgp/tracks",
                "total": 0
            }
        },
        {
            "id": "5W4MSiW8ajkUdSDIxDSilb",
            "name": "Hapi Facade Playlist",
```
and an environment variable `playlistId` needs to be set with the id of the first item from items array then the test should look like this
```
   ENVIRONMENT VARIABLE                      RESPONSE PATH
   playlistId                                response.items[0].id
```

#### Create your own Spotify App

This section walks you through to create your own Spotify app with respective client key and secret.

1. Navigate to `https://developer.spotify.com/dashboard/login` and login with your Spotify account.
2. Click on "Create New App" or "Create a Client ID".   Fill out the relevant fields and agree to the Spotify Developer
   license terms.
3. You will be taken to the App dashboard for the newly created app.  Click on "Show Client Secret" and note the client
   id and secret.
   ![Spotify Id and Secret](./images/spotify-keys.png)
4. Click on "Settings" and add a redirect URL of your choice like `http://localhost/redirect`.  This is just a dummy
   redirect for this demo since we will be retrieving the auth code manually (see Spotify Quickstart for reference).
   ![Spotify Settings for Redirect](./images/spotify-redirect.png)
5. Use a base64 encoder to encode your client id and secret in the following string:
     
     `[clientid]:[clientsecret]`
   
   This will be used as the authtoken during Spotify login.  
6. Construct your authorize url in the following manner:
     `https://accounts.spotify.com/authorize?client_id={$clientid}&response_type=code&scope=playlist-read-private playlist-modify-private&redirect_uri={$redirectUri}&show_dialog=true`
7. You may then use the authorize url to get the authcode as per the Spotify Quickstart in previous section.
