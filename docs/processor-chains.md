# Processor Chains

Every Facade API endpoint will configure a processor chain of 1 or more processors.
First let's decipher a basic Facade API endpoint

```json
{
  "endpoints": [
    {
      "uri": "/weather/locationsearch",
      "method": "get",
      "processors": [
        {
          "name": "weatherLocationSearch",
          "type": "api",
          "uri": "https://www.metaweather.com/api/location/search/?query=<%=request.query.location%>",
          "request": {
            "method": "get"
          }
        }
      ]
    },
}
```

This endpoint will be triggered on any `GET` request to `{base}/weather/locationsearch`
