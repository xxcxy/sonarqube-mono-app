## Publishing an updated lambda

Follow these steps to publish an update to a Lambda function. In this demo, we'll update publish a new Lambda function, and then create a new endpoint in Facade API to point to the new Lambda function.

1. Run `docker-compose up` to start the service.

1. Once the services are up, go to https://prod.local.com/ and https://uat.local.com in an incognito tab of your browser.

1. Press the `Import` button, and import the `demo-apis.json` file which is located in the `docs` folder.

<img alt="Import File" src="./images/import-file.png" width="800">

1. Go to `Demo Collection` > `basic-usage` > `Passthrough With Lambda Example`. When the request template is loaded, hit the `Send` button.

<img alt="Send Request" src="./images/send-request.png" width="800">

---

<!-- ![Send Request](./images/send-request.png) -->

1. Note the response you get from the endpoint.

    ```json
    [
        {
            "title": "London",
            "location_type": "City",
            "woeid": 44418,
            "latitude": "51.506321",
            "longitude": "-0.12714"
        }
    ]
    ```

1. Create a new lambda endpoint. [Lambda Functions](./lambda-functions.md) describes the process. For the purpose of this demo, assume that the lambda endpoint `facade/latLongV2` is the newly-published endpoint. To enable the lambda, just uncomment the following code block in `apps/lambda-services/serverless.yml`:

    ```yml
      latLongV2:
        handler: main.latLongV2
        events:
          - http:
              path: facade/latlongV2
              method: post
              cors: true
    ```

1. We must now make changes to the facade config to ensure that it points to the updated lambda in the UAT environment. In `libs/app-config/src/sample-facade-config/facade.yaml`, extract the value of the uri endpoint to a string variable and substitute that variable in place of the actual uri in the uri field of the processor to be updated. Use the `environments` section of the config file to set the proper value of the endpoint based on the environment. The uri for `latLongNormalizer` processor, on hydration, will come out to be:

    - http://localhost:3000/facade/latlongV2 in `local/facade.json`
    - http://serverless:3000/facade/latlongV2 in `uat/facade.json`
    - http://serverless:3000/facade/latlong in `prod/facade.json`

1.    The following snippet shows what the changes look like to update the `/weather/locationsearch/latlong` for this demo.

    ```yml
    default: !!map
      _latLongLambdaEndpoint: !!str "facade/latlong"
      _lambdaBaseUri: !!str "http://serverless:3000/dev"

      ...
      endpoints: !!seq
        ...
        # "/weather/locationsearch/latlong
        -  !!map
          uri: !!str "/weather/locationsearch/latlong"
          method: !!str "get"
          processors: !!seq
            ...
            - !!map
              name: !!str "latLongNormalizer"
              ...
              uri: !!str "${_lambdaBaseUri}/${_latLongLambdaEndpoint}"

      environments: !!map
        local: !!map
          _lambdaBaseUri: !!str "http://localhost:3000"
          _latLongLambdaEndpoint: !!str "facade/latlongV2"

        uat: !!map
          _lambdaBaseUri: !!str "http://serverless:3000"
          _latLongLambdaEndpoint: !!str "facade/latlongV2"

        prod: !!map
          _lambdaBaseUri: !!str "http://serverless:3000"
          _latLongLambdaEndpoint: !!str "facade/latlong"
    ```

1. Run `npm run hydrate` to apply the changes made to the facade file.

1. Open [https://uat.local.com](https://uat.local.com), and follow the same steps (import the `demo-apis.json`, go to `Demo Collection` > `basic-usage` > `Passthrough With Lambda Example`, and press the `Send` button. This will fetch data from the new lambda function, even though the facade endpoint will be the same (`weather/locationsearch/latlong`). The data will pass through the updated lambda function processor and would return a different result.

     <img alt="Sending request from the UAT version of the app" src="./images/send-request-uat.png" width="800">

1. The response would like something like:

    ```json
    [
        {
            "title": "London",
            "location_type": "City",
            "woeid": 44418,
            "latt_long": "51.506321,-0.12714",
            "latitude": "51.506321",
            "longitude": "-0.127140",
            "info": {
                "address_components": [
                    {
                        "long_name": "Whitehall Trafalgar Square",
                        "short_name": "Whitehall Trafalgar Square",
                        "types": [
                            "establishment",
                            "point_of_interest",
                            "transit_station"
                        ]
                    },
                    {
                        "long_name": "Westminster",
                        "short_name": "Westminster",
                        "types": [
                            "neighborhood",
                            "political"
                        ]
                    },
                    {
                        "long_name": "London",
                        "short_name": "London",
                        "types": [
                            "postal_town"
                        ]
                    },
                    {
                        "long_name": "Greater London",
                        "short_name": "Greater London",
                        "types": [
                            "administrative_area_level_2",
                            "political"
                        ]
                    },
                    {
                        "long_name": "England",
                        "short_name": "England",
                        "types": [
                            "administrative_area_level_1",
                            "political"
                        ]
                    },
                    {
                        "long_name": "United Kingdom",
                        "short_name": "GB",
                        "types": [
                            "country",
                            "political"
                        ]
                    },
                    {
                        "long_name": "SW1A",
                        "short_name": "SW1A",
                        "types": [
                            "postal_code",
                            "postal_code_prefix"
                        ]
                    }
                ],
                "formatted_address": "Whitehall Trafalgar Square, Westminster, London SW1A, UK",
                "geometry": {
                    "location": {
                        "lat": 51.5063498,
                        "lng": -0.1270315
                    },
                    "location_type": "GEOMETRIC_CENTER",
                    "viewport": {
                        "northeast": {
                            "lat": 51.50769878029149,
                            "lng": -0.125682519708498
                        },
                        "southwest": {
                            "lat": 51.5050008197085,
                            "lng": -0.128380480291502
                        }
                    }
                },
                "place_id": "ChIJWZC8us8EdkgRXbs89BRdhmU",
                "plus_code": {
                    "compound_code": "GV4F+G5 Westminster, London, United Kingdom",
                    "global_code": "9C3XGV4F+G5"
                },
                "types": [
                    "establishment",
                    "point_of_interest",
                    "transit_station"
                ]
            }
        }
    ]
    ```

1. Then, to update the `prod` environment, update the value of the `_latLongLambdaEndpoint` variable so that it points to the updated lambda function.

    ```yml
    default: !!map
      ...

      environments: !!map
        ...
        prod: !!map
          _lambdaBaseUri: !!str "http://serverless:3000"
          _latLongLambdaEndpoint: !!str "facade/latlongV2"
    ```

1. Run `npm run hydrate` to apply the changes.
