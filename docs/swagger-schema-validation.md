# About Mocha Tests
This document shows how to write and run Mocha tests.

# Run Tests
In the project root, run: 
```
npm run facade-swagger-test
```
It will run all the tests written in `apps/facade-swagger-test/tests/*.spec.ts`

# Facade API Base URL

_NOTE_: before running the tests, you should run the facade-service first. 
In the sample tests in `apps/facade-swagger-test/tests/*.spec.ts`, it uses the facade-service default prefix: `/api/facade"`. If your prefix changed, please search all `/api/facade` and inside the ``apps/facade-swagger-test/tests` folder and replace them all.

# Create Swagger Schema Validation API Tests

Refer to this sample test: [facade-location-apis.spec.ts](../apps/facade-swagger-test/tests/facade-location-apis.spec.ts) for reference.

1. Define the common configuration for the tests.

    ```typescript
      const swaggerRootDir = path.resolve('libs/shared/api');

      const commonConfig = {
          baseURL: 'https://ui.local.com:4210/api/facade',
          authURL: 'https://ui.local.com:4210/api/auth'
      };

      const commonProtectedConfig = {
          ...commonConfig,
          loginURL: 'https://ui.local.com:4210/api/login',
          loginUsername: appConfig.USERS[0].USERNAME,
          loginPassword: appConfig.USERS[0].PASSWORD,
          protected: true
      };
    ```

1. Create a test case. The actual test cases go inside the `it` function. `describe` is used to organize the test cases into test suites.

    ```typescript
      describe('Parent Test Suite', () => {
        describe('Child Test Suite', () => {
          it('Test Case', () => {
            // Test case will go here
          })
        };
      };
    ```

1. A test suite in our case will look like this:

    ```typescript
      describe('Facade Location API verification with YAML swagger', () => {
        describe('Lat-long API with Location Search (Unauthenticated)', () => {
          const request = {
              // Request config goes here
            };
          it('should return 200 with formatted body', () => {
            // Test case will go here
          })
          it('should return 400 when proper parameters aren\'t passed ', () => {
            // Test case will go here
          })
        };
      };
    ```

1. A test case looks like this:

    ```typescript
    describe('Lat-long API with Location Search (Unauthenticated)', () => {
        // This is an axios request config
        const request = {
            method: 'GET',
            url: '/weather/locationsearch/latlong',
            params: {
                location: 'London'
            }
        };

        it('should return 200 with formatted body', async () => {
            // Response must return with this status code
            const expectedStatusCode = 200;
            // Create a wrapper around the swagger file for our endpoint
            // with the proper configuration
            const swaggerAPI = new SwaggerAPI({
               // endpoint will use this base url
              baseURL: 'https://ui.local.com:4210/api/facade',
               // Anonymous token will be generated through this endpoint
              authURL: 'https://ui.local.com:4210/api/auth'
              // Path to the swagger file
              swaggerFile: path.join(swaggerRootDir, 'locationsearch-latlong.openapi.yaml')
              // Whether the endpoint is protected.
              // If this is false, none of the following parameters are required.
              protected: true,
              // ID Token will be generated using this endpoint
              loginURL: 'https://ui.local.com:4210/api/login',
              // Username to be used to generate ID token
              loginUsername: appConfig.USERS[0].USERNAME,
              // Password to be used to generate ID token
              loginPassword: appConfig.USERS[0].PASSWORD,
            });
            // Fetch the expected result from the swagger
            const expectedResponse = await swaggerAPI.getSchemaFor(request.method, request.url, expectedStatusCode);
            // Call the API and save the result
            const response = await swaggerAPI.send(request);
            // Test the response
            expect(response.status).to.equal(200);
            expect(response.data).to.be.a['jsonSchema'](expectedResponse);
        });
    });
    ```
