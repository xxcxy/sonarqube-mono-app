# Lambda Functions

Each lambda function is an individual library placed inside the `libs/shared/lambda` path.

For a lambda function named `latt-long`:
- the function is defined at `libs/shared/lambda/latt-long/src/index.ts`.
- the `serverless.yml` file is defined at `libs/shared/lambda/latt-long/serverless.yml`.

### Running tests

Use the following command to run tests (replace `latt-long` with the name of the lambda library).

```sh
ng test latt-long
```

### Creating a new Lambda function

To create a new lambda function named `lambdaFn`, do the following:

1. Run the following commands:
    
    ```sh
    $ ng g @nrwl/node:library --name=lambda-fn --directory=shared/lambda --unitTestRunner=none
    $ cd libs/shared/lambda/lambda-fn
    $ rm -rf src/lib
    ```

1. Rewrite the contents of `libs/shared/lambda/lambda-fn/src/index.ts` to export the lambda function.

    ```js
    export async function lambdaFn(event) {
        return {
            status: 200,
            body: JSON.stringify({
                'message': 'Hello from Lambda!'
            })
        };
    }
    ```

1. Create a file named `serverless.yml` at `libs/shared/lambda/lambda-fn/serverless.yml`

    ```yml
    service: lambda-services-lambda-fn

    provider:
    name: aws
    runtime: nodejs12.x
    webpack:
        webpackConfig: 'webpack.serverless.config.js'
        includeModules: true
        packager: 'npm'
        packExternalModulesMaxBuffer: 204800

    functions:
        lambdaFn:
            handler: src/index.lambdaFn
            events:
            - http:
                path: facade/lambdaFn
                method: post
                cors: true

    plugins:
    - serverless-offline
    - serverless-webpack

    custom:
    serverless-offline:
        host: 0.0.0.0
    ```

1. Create a file named `webpack.config.js` in the directory `libs/shared/lambda/lambda-fn`.

    ```js
    const config = require('../../../../webpack.serverless.config');

    module.exports = config
    ```

1. To deploy the lambda function, run the following commands:
    ```sh
    npm i -g serverless
    cd libs/shared/lambda/lambda-fn && sls deploy
    ```

For running these functions offline, a `lambda-services` app has been created. Use the following instruction to configure the app:

1. Import the `lambda-fn` in the file `apps/lambda-services/src/main.ts` by adding the following line to the end of the file:

    ```js
    export { lambdaFn } from '@pop-soda-facade/shared/lambda/lambda-fn'
    ```

1. Add the following section to the `functions` section of the `serverless.yml` file located at `apps/lambda-services/src/serverless.yml`.

    ```yml
    ...
    functions:
        ...
        lambdaFn:
            handler: main.lambdaFn
            events:
            - http:
                path: spotify/tracks/lambdaFn
                method: post
                cors: true
    ```
