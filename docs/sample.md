


# Hapi Facade Service Call Log

## initial API request

```bash
pop-facade   | 2020-03-28T01:56:34.410Z [info]:  GET  /weatherpop/locationsearch
```
## Authenticate Request

facade-service core libs authenticates soda pop signature of all requests

```bash
pop-facade    | 2020-03-28T01:56:34.410Z [info]: Authenticating the request<br/>
pop-facade    | 2020-03-28T01:56:34.411Z [debug]: Auth headers:
```

### _Auth headers_

```json
{
   "host":"localhost:4210",
   "connection":"keep-alive",
   "accept":"application/json, text/plain, */*",
   "sec-fetch-dest":"empty",
   "x-nonce": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImp3ayI6eyJrdHkiOiJFQyIsImNydiI6IlAtMjU2IiwieCI6IlZTc0ZnQk9nTE9hQmNmX1lmY3FZWmRTeDJ1c1lGZjdzX05BZEtSQnBXYk0iLCJ5IjoidGtHeUxORjlVaWZLaWRBZTliQW5EcXB5c1NfUXBTeUVuVTd1MUl4aEVsayJ9LCJraWQiOiJycjd4aEJUaFBxcVQwNVZ2cEhMMGVZenU1aUh4dmtza1VmbC16bXRTMHdnIn0.eyJlaHRzIjpbIm1ldGhvZCIsInVyaSJdLCJlZHRzIjoiY2E4Mzg2NTVjZWQzNjI4MDA3Njg4ZWJkOGRjYTUxNDk5MDgwNjNmY2NlMGQ5YmIzNTY4MDQzZTQzMTczOTFjMCJ9.c4WMbxwy0AEiidqpgimwbtU114dcKZMwnoCacE6_P6q_rPF20L_nX3BEURqp5fUkeofSY5FHeoRec7Qf2x2nJA",
   "x-wid":"9339d480-852e-401a-8e33-fe3fd3be82aa",
   "user-agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36",
   "dnt":"1",
   "sec-fetch-site":"same-origin",
   "sec-fetch-mode":"cors",
   "referer":"http://localhost:4210/dashboard",
   "accept-encoding":"gzip, deflate, br",
   "accept-language":"en-US,en;q=0.9",
   "cookie":"a_token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnRfYXBwXzEiLCJ1c24iOiI1OTFhNmNlNy05NDJkLTQwMGEtOWQxZi1mNTk5NjMxMTdlZDMiLCJjbmYiOnsibG9jYWxob3N0Ijp7ImNydiI6IlAtMjU2IiwiZXh0Ijp0cnVlLCJrZXlfb3BzIjpbInZlcmlmeSJdLCJrdHkiOiJFQyIsIngiOiJWU3NGZ0JPZ0xPYUJjZl9ZZmNxWVpkU3gydXNZRmY3c19OQWRLUkJwV2JNIiwieSI6InRrR3lMTkY5VWlmS2lkQWU5YkFuRHFweXNTX1FwU3lFblU3dTFJeGhFbGsifX0sImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDIxMC8iLCJleHAiOjE1ODU0Mzc3NzksImlhdCI6MTU4NTM1MTM3OX0.Lz2JVctMyyYglQ_Jmqn8v1TDpYnBMEqVZAQ2KiQdoRN2ZT6t6_QpAhJZiBefeZn8oyJXLpNr00P1pUkUJCyxqOESiSe9SGAWvbHP4YaNIhRjGuHgSeEHvxnMjXJClt6peJSXCjRXNDY3INZUcdH2GrI1u_i3S8w-P3frhjdMpHR4nVmB9i9SEGHL4ZYf6xmTwgX-KdYyvUSkCUiVAZNnsYNTh1ID9mwVWhN4qD_1NPwl0bDeBZh1SWz1z4mDYev9onI7oGZMJUxCVKs_cQ55XJy_cfDuYxotIkTlCLq_e-cMGXz9I9TFzO4Fj3DlMCWbL4MDUTTaG9BPqlEWu3fYOg"
}
```

**_anonymous pop token cookie: `a_token`_**
```json
   "cookie":"a_token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnRfYXBwXzEiLCJ1c24iOiI1OTFhNmNlNy05NDJkLTQwMGEtOWQxZi1mNTk5NjMxMTdlZDMiLCJjbmYiOnsibG9jYWxob3N0Ijp7ImNydiI6IlAtMjU2IiwiZXh0Ijp0cnVlLCJrZXlfb3BzIjpbInZlcmlmeSJdLCJrdHkiOiJFQyIsIngiOiJWU3NGZ0JPZ0xPYUJjZl9ZZmNxWVpkU3gydXNZRmY3c19OQWRLUkJwV2JNIiwieSI6InRrR3lMTkY5VWlmS2lkQWU5YkFuRHFweXNTX1FwU3lFblU3dTFJeGhFbGsifX0sImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDIxMC8iLCJleHAiOjE1ODU0Mzc3NzksImlhdCI6MTU4NTM1MTM3OX0.Lz2JVctMyyYglQ_Jmqn8v1TDpYnBMEqVZAQ2KiQdoRN2ZT6t6_QpAhJZiBefeZn8oyJXLpNr00P1pUkUJCyxqOESiSe9SGAWvbHP4YaNIhRjGuHgSeEHvxnMjXJClt6peJSXCjRXNDY3INZUcdH2GrI1u_i3S8w-P3frhjdMpHR4nVmB9i9SEGHL4ZYf6xmTwgX-KdYyvUSkCUiVAZNnsYNTh1ID9mwVWhN4qD_1NPwl0bDeBZh1SWz1z4mDYev9onI7oGZMJUxCVKs_cQ55XJy_cfDuYxotIkTlCLq_e-cMGXz9I9TFzO4Fj3DlMCWbL4MDUTTaG9BPqlEWu3fYOg"
```

**_request `nonce`_**
```json
eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImp3ayI6eyJrdHkiOiJFQyIsImNydiI6IlAtMjU2IiwieCI6IlZTc0ZnQk9nTE9hQmNmX1lmY3FZWmRTeDJ1c1lGZjdzX05BZEtSQnBXYk0iLCJ5IjoidGtHeUxORjlVaWZLaWRBZTliQW5EcXB5c1NfUXBTeUVuVTd1MUl4aEVsayJ9LCJraWQiOiJycjd4aEJUaFBxcVQwNVZ2cEhMMGVZenU1aUh4dmtza1VmbC16bXRTMHdnIn0.eyJlaHRzIjpbIm1ldGhvZCIsInVyaSJdLCJlZHRzIjoiY2E4Mzg2NTVjZWQzNjI4MDA3Njg4ZWJkOGRjYTUxNDk5MDgwNjNmY2NlMGQ5YmIzNTY4MDQzZTQzMTczOTFjMCJ9.c4WMbxwy0AEiidqpgimwbtU114dcKZMwnoCacE6_P6q_rPF20L_nX3BEURqp5fUkeofSY5FHeoRec7Qf2x2nJA
```


#### Decoded tokens

**_`a_token` pop jwt_**

```json
{
  "aud": "client_app_1",
  "usn": "591a6ce7-942d-400a-9d1f-f59963117ed3",
  "cnf": {
    "localhost": {
      "crv": "P-256",
      "ext": true,
      "key_ops": [
        "verify"
      ],
      "kty": "EC",
      "x": "VSsFgBOgLOaBcf_YfcqYZdSx2usYFf7s_NAdKRBpWbM",
      "y": "tkGyLNF9UifKidAe9bAnDqpysS_QpSyEnU7u1IxhElk"
    }
  },
  "iss": "http://localhost:4210/",
  "exp": 1585437779,
  "iat": 1585351379
}
```

**_`nonce jwt`_**
```json
{
  "ehts": [
    "method",
    "uri"
  ],
  "edts": "ca838655ced3628007688ebd8dca5149908063fcce0d9bb3568043e4317391c0"
}
```

```bash
pop-facade    | 2020-03-28T01:56:34.433Z [debug]: Generated digest: `gethttp://localhost:4210/weatherpop/locationsearch?location=London`
```

-- encode digest and compare to nonce token to validate request

## Begin Processor Chain

1. [Processor 1: weatherLocationSearch](#processor-1-weatherlocationsearch)
2. [Processor 2: teleportLocationSearch](#processor-2-teleportlocationsearch)
3. [Processor 3: transformer {aggregator}](#processor-1-transformer--aggregator-)

### Processor 1: weatherLocationSearch

```bash
pop-facade    | 2020-03-28T01:56:34.435Z [info]: Starting API processor weatherLocationSearch:  GET https://www.metaweather.com/api/location/search/?query=London

pop-facade    | 2020-03-28T01:56:34.435Z [info]: with options:  {"url":"https://www.metaweather.com/api/location/search/?query=London","method":"get"}
```

### Processor 2:  teleportLocationSearch

```bash
- pop-facade    | 2020-03-28T01:56:34.435Z [info]: Starting API processor **teleportLocationSearch**:
    - **_GET_** `https://api.teleport.org/api/cities/?search=London`
- pop-facade    | 2020-03-28T01:56:34.436Z [info]: with options:
    - `{"url":"https://api.teleport.org/api/cities/?search=London","method":"get"}`
- pop-facade    | 2020-03-28T01:56:34.512Z [info]: API Processor: **teleportLocationSearch** executed successfully with result:
```

```json
{
  "body": {
    "_embedded": {
      "city:search-results": [
        {
          "_links": {"city:item": {"href": "https://api.teleport.org/api/cities/geonameid:2643743/"}},
          "matching_alternate_names": [
            { "name": "london"},
            { "name": "London"},
            { "name": "London osh"},
            { "name": "Londona"},
            { "name": "Londonas"},
            { "name": "Londoni"},
            { "name": "londoni"},
            { "name": "Londono"},
            { "name": "Londons"},
            { "name": "Londonu"}
          ],
          "matching_full_name": "London, England, United Kingdom"
        },
        {
          "_links": {"city:item": {"href": "https://api.teleport.org/api/cities/geonameid:6058560/"}
          },
          "matching_alternate_names": [
            {"name": "London"},
            {"name": "Londonas"},
            {"name": "londoni"},
            {"name": "Londono"}
          ],
          "matching_full_name": "London, Ontario, Canada"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:2643734/"}
          },
          "matching_alternate_names": [
            {"name": "Londonderry County Borough"}
          ],
          "matching_full_name": "Londonderry County Borough, Northern Ireland, United Kingdom"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:2655775/"}
          },
          "matching_alternate_names": [
            {"name": "London Borough of Bexley"},
            {"name": "Londonska opshtina Beksli"}
          ],
          "matching_full_name": "Bexley, England, United Kingdom (London Borough of Bexley)"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:2636503/"}
          },
          "matching_alternate_names": [
            {"name": "London Borough of Sutton"}
          ],
          "matching_full_name": "Sutton, England, United Kingdom (London Borough of Sutton)"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:2643736/"}
          },
          "matching_alternate_names": [
            {"name": "Londonderis"},
            {"name": "Londonderry"}
          ],
          "matching_full_name": "Derry, Northern Ireland, United Kingdom (Londonderis)"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:5088905/"}
          },
          "matching_alternate_names": [
            {"name": "Londondehrri"},
            {"name": "Londonderi"},
            {"name": "Londonderri"},
            {"name": "Londonderry"}
          ],
          "matching_full_name": "Londonderry, New Hampshire, United States"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:4517009/"}
          },
          "matching_alternate_names": [
            {"name": "London"}
          ],
          "matching_full_name": "London, Ohio, United States"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:4298960/"}
          },
          "matching_alternate_names": [
            {"name": "London"}
          ],
          "matching_full_name": "London, Kentucky, United States"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:4361094/"}
          },
          "matching_alternate_names": [
            {"name": "London Town"},
            {"name": "London Towne"},
            {"name": "Londontown"},
            {"name": "Londontowne"}
          ],
          "matching_full_name": "Londontowne, Maryland, United States"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:2656295/"}
          },
          "matching_alternate_names": [
            {"name": "London Borough of Barnet"},
            {"name": "Londonska opshtina Barnet"}
          ],
          "matching_full_name": "Barnet, England, United Kingdom (London Borough of Barnet)"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:5367815/"}
          },
          "matching_alternate_names": [{"name": "London"}],
          "matching_full_name": "London, California, United States"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:4030939/"}
          },
          "matching_alternate_names": [ {"name": "London"}, { "name": "London Village"}],
          "matching_full_name": "London Village, Line Islands, Kiribati"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:5238077/"}
          },
          "matching_alternate_names": [{"name": "Londonderry"}],
          "matching_full_name": "Londonderry, Vermont, United States"
        },
        {
          "_links": {
            "city:item": {
              "href": "https://api.teleport.org/api/cities/geonameid:2643741/"
            }
          },
          "matching_alternate_names": [
            { "name": "London"},
            { "name": "Londonas Sitija"},
            { "name": "Londono Sitis"},
            { "name": "Londons'ke Siti"},
            { "name": "Londons'ke siti"}
          ],
          "matching_full_name": "City of London, England, United Kingdom (London)"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:4119617/"}
          },
          "matching_alternate_names": [
            {"name": "London"}
          ],
          "matching_full_name": "London, Arkansas, United States"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:5161340/"}
          },
          "matching_alternate_names": [
            {"name": "Londonville"}
          ],
          "matching_full_name": "Loudonville, Ohio, United States (Londonville)"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:2650567/"}
          },
          "matching_alternate_names": [
            {"name": "London Borough of Ealing"}
          ],
          "matching_full_name": "Ealing, England, United Kingdom (London Borough of Ealing)"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:2646517/"}
          },
          "matching_alternate_names": [
            {"name": "London Borough of Hounslow"}
          ],
          "matching_full_name": "Hounslow, England, United Kingdom (London Borough of Hounslow)"
        },
        {
          "_links": {
            "city:item": {"href": "https://api.teleport.org/api/cities/geonameid:2634812/"}
          },
          "matching_alternate_names": [
            {"name": "London Borough of Wandsworth"}
          ],
          "matching_full_name": "Wandsworth, England, United Kingdom (London Borough of Wandsworth)"
        }
      ]
    },
    "_links": {
      "curies": [
        {
          "href": "https://developers.teleport.org/api/resources/Location/#!/relations/{rel}/",
          "name": "location",
          "templated": true
        },
        {
          "href": "https://developers.teleport.org/api/resources/City/#!/relations/{rel}/",
          "name": "city",
          "templated": true
        },
        {
          "href": "https://developers.teleport.org/api/resources/UrbanArea/#!/relations/{rel}/",
          "name": "ua",
          "templated": true
        },
        {
          "href": "https://developers.teleport.org/api/resources/Country/#!/relations/{rel}/",
          "name": "country",
          "templated": true
        },
        {
          "href": "https://developers.teleport.org/api/resources/Admin1Division/#!/relations/{rel}/",
          "name": "a1",
          "templated": true
        },
        {
          "href": "https://developers.teleport.org/api/resources/Timezone/#!/relations/{rel}/",
          "name": "tz",
          "templated": true
        }
      ],
      "self": {
        "href": "https://api.teleport.org/api/cities/?search=London&geohash="
      }
    },
    "count": 20
  },
  "headers": {
    "content-type": "application/json; charset=utf-8",
    "transfer-encoding": "chunked",
    "connection": "close",
    "access-control-max-age": "600",
    "cache-control": "public, max-age=300",
    "server": "nginx",
    "x-apiproxy-cache-status": "MISS",
    "date": "Sat, 28 Mar 2020 01:56:13 GMT",
    "via": "1.1 google, 1.1 f126db770f21f288439f44d357978a27.cloudfront.net (CloudFront)",
    "vary": "Origin,Accept-Encoding",
    "x-cache": "Hit from cloudfront",
    "x-amz-cf-pop": "SEA19-C1",
    "x-amz-cf-id": "T7R2BrGbAlgLzDkpk3Bn-M0gT1aFnvFrrzDj8qQ4eh5kDdyyT5k0fQ==",
    "age": "21"
  }
}
```


```bash
pop-facade    | 2020-03-28T01:56:35.303Z [info]: API Processor: weatherLocationSearch executed successfully with result:
```

```json
{
  "body":[
    {
      "title":"London",
      "location_type":"City",
      "woeid":44418,
      "latt_long":"51.506321,-0.12714"
    }
  ],
  "headers":{
    "x-xss-protection":"1; mode=block",
    "content-language":"en",
    "x-content-type-options":"nosniff",
    "strict-transport-security":"max-age=2592000; includeSubDomains",
    "vary":"Accept-Language, Cookie",
    "allow":"GET, HEAD, OPTIONS",
    "x-frame-options":"DENY",
    "content-type":"application/json",
    "x-cloud-trace-context":"1dd7480e3b4b7be24c5b69a514d58769",
    "date":"Sat, 28 Mar 2020 01:56:35 GMT",
    "server":"Google Frontend",
    "content-length":"90",
    "connection":"close"
  }
}
```

### Processor 3: transformer { aggregator }
```bash
pop-facade    | 2020-03-28T01:56:35.303Z [info]: Starting transfomer processor: aggregator
pop-facade    | 2020-03-28T01:56:35.304Z [info]: Transfomer processor: aggregator executed successfully with
```

```json
result: {
  "body":{
     "weatherResult":[
       {
         "title":"London",
         "location_type":"City",
         "woeid":44418,
         "latt_long":"51.506321,-0.12714"
       }
     ],
     "teleportResult":"London, England, United Kingdom"
   }
 }
```

## Initial API Response
```bash
pop-facade    | [2020-03-28T01:56:35.306Z       : API:   GET `/weatherpop/locationsearch`
pop-facade    | [2020-03-28T01:56:35.306Z       : API:   GET `/weatherpop/locationsearch` |
```
```json
Headers: {
  "host":"localhost:4210",
  "connection":"keep-alive",
  "accept":"application/json, text/plain, */*",
  "sec-fetch-dest":"empty",
  "x-nonce": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImp3ayI6eyJrdHkiOiJFQyIsImNydiI6IlAtMjU2IiwieCI6IlZTc0ZnQk9nTE9hQmNmX1lmY3FZWmRTeDJ1c1lGZjdzX05BZEtSQnBXYk0iLCJ5IjoidGtHeUxORjlVaWZLaWRBZTliQW5EcXB5c1NfUXBTeUVuVTd1MUl4aEVsayJ9LCJraWQiOiJycjd4aEJUaFBxcVQwNVZ2cEhMMGVZenU1aUh4dmtza1VmbC16bXRTMHdnIn0.eyJlaHRzIjpbIm1ldGhvZCIsInVyaSJdLCJlZHRzIjoiY2E4Mzg2NTVjZWQzNjI4MDA3Njg4ZWJkOGRjYTUxNDk5MDgwNjNmY2NlMGQ5YmIzNTY4MDQzZTQzMTczOTFjMCJ9.c4WMbxwy0AEiidqpgimwbtU114dcKZMwnoCacE6_P6q_rPF20L_nX3BEURqp5fUkeofSY5FHeoRec7Qf2x2nJA",
  "x-wid":"9339d480-852e-401a-8e33-fe3fd3be82aa",
  "user-agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36",
  "dnt":"1",
  "sec-fetch-site":"same-origin",
  "sec-fetch-mode":"cors",
  "referer":"http://localhost:4210/dashboard",
  "accept-encoding":"gzip, deflate, br",
  "accept-language":"en-US,en;q=0.9",
  "cookie": "a_token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnRfYXBwXzEiLCJ1c24iOiI1OTFhNmNlNy05NDJkLTQwMGEtOWQxZi1mNTk5NjMxMTdlZDMiLCJjbmYiOnsibG9jYWxob3N0Ijp7ImNydiI6IlAtMjU2IiwiZXh0Ijp0cnVlLCJrZXlfb3BzIjpbInZlcmlmeSJdLCJrdHkiOiJFQyIsIngiOiJWU3NGZ0JPZ0xPYUJjZl9ZZmNxWVpkU3gydXNZRmY3c19OQWRLUkJwV2JNIiwieSI6InRrR3lMTkY5VWlmS2lkQWU5YkFuRHFweXNTX1FwU3lFblU3dTFJeGhFbGsifX0sImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDIxMC8iLCJleHAiOjE1ODU0Mzc3NzksImlhdCI6MTU4NTM1MTM3OX0.Lz2JVctMyyYglQ_Jmqn8v1TDpYnBMEqVZAQ2KiQdoRN2ZT6t6_QpAhJZiBefeZn8oyJXLpNr00P1pUkUJCyxqOESiSe9SGAWvbHP4YaNIhRjGuHgSeEHvxnMjXJClt6peJSXCjRXNDY3INZUcdH2GrI1u_i3S8w-P3frhjdMpHR4nVmB9i9SEGHL4ZYf6xmTwgX-KdYyvUSkCUiVAZNnsYNTh1ID9mwVWhN4qD_1NPwl0bDeBZh1SWz1z4mDYev9onI7oGZMJUxCVKs_cQ55XJy_cfDuYxotIkTlCLq_e-cMGXz9I9TFzO4Fj3DlMCWbL4MDUTTaG9BPqlEWu3fYOg"
}
```

```bash
pop-facade    | [2020-03-28T01:56:35.306Z       : API:   GET `/weatherpop/locationsearch` | Response:
```
```json
{
  "weatherResult":[
    {
      "title":"London",
      "location_type":"City",
      "woeid":44418,
      "latt_long":"51.506321,-0.12714"
    }  
  ],
  "teleportResult":"London, England, United Kingdom"
}
```
