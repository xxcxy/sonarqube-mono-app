# Facade API Demo

`docker-compose up --build`

show docker dashboard with running services.

### Open both Soda Pop Postman ui
- [UAT: Soda Pop Postman](https://uat.local.com)
- [PROD: Soda Pop Postman](https://prod.local.com)

- Open the dev tools docked to the bottom of the page
- click on the _Network_ tab and hit `esc` to show the console drawer.
- Load the demo client api into each postman instance. `docs/demo/demo-apis.json`
- show api description for Simple Passthru example
- 
