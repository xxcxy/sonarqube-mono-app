## Auto-generating models/interfaces from OpenAPI files

1. Place a new swagger file in the [libs/shared/api/src/lib/facade](../libs/shared/api/src/lib/facade) directory.

1. Run `npm run code-autogen` command to auto-generate API models, interfaces and Angular-consumable code that can be used to call these APIs for all the swagger files in the directory. Assuming the swagger file's name is `api.locationsearch.yaml`,

    - The auto-generated code will be available in the [libs/shared/api/src/lib/generated/api.locationsearch/](../libs/shared/api/src/lib/generated/api.locationsearch) directory.
    - The models will be available in the [libs/shared/api/src/lib/generated/api.locationsearch/model](../libs/shared/api/src/lib/generated/api.locationsearch/model) directory.

1. The auto-generated code will likely have a few linting errors. Run `ng lint --fix` to list those errors, and possibly have the linter automatically fix a few of the errors.

1. This example will detail the changes for the `latt-long` lambda transformer that is located at [libs/shared/lambda/latt-long](libs/shared/lambda/latt-long).

1. To consume the models, we'll first have to export the model from the umbrella library. For this example, we'll be be exporting the `Location` model from the `MetaWeather API` swagger file (located [here](../libs/shared/api/src/lib/facade/api.metaweather.yaml)), and the `Location` model from the `LatLong Lambda Function API` (located [here](../libs/shared/api/src/lib/facade/api.locationsearch-latlong.yaml)).

1. To consume the model from the wider codebase, the modal will need to be exported from the `@pop-soda-facade/shared/api` library. To do so, open the file [index.ts](../libs/shared/api/src/index.ts) which is located at in the [libs/shared/api/src/index.ts](../libs/shared/api/src/index.ts) directory.

1. Add the following two lines to the file:

    ```ts
    export { Location as MetaWeatherResponse } from './lib/generated/api.metaweather/model/location';
    export { LocationV2 as LambdaLocationV2Response } from './lib/generated/api.locationsearch-latlong/model/locationV2';
    ```

    This will export two interfaces: `MetaWeatherResponse` and `LambdaLocationV2Response`, which can be consumed from the wider codebase.

1. To consume the interfaces from the `latt-long` Lambda, open the [index.ts](../libs/shared/lambda/latt-long/src/index.ts) file located inside the [libs/shared/lambda/latt-long/src](../libs/shared/lambda/latt-long/src/index.ts) directory, and import the models.

    ```ts
    import { MetaWeatherResponse, LambdaLocationV2Response } from '@pop-soda-facade/shared/api';
    ```

1. Use these interfaces to decorate the requests, responses or other objects. Example:

    ```ts
    // const weatherLocation = JSON.parse(event.body);
    const weatherLocation: Array<MetaWeatherResponse> = JSON.parse(event.body);
    // const responseBody = [];
    const responseBody: Array<LambdaLocationV2Response> = [];
    ```